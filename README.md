Balok: a data-race detection runtime
====================================


Usage
-----


1. Install `balok-1.0-dev` in your *local* Maven repository.

       ./gradlew install

2. Install `balok-1.0-dev` in a *remote* Maven repository.

       ./gradlew installRemote

Dependencies
------------

Gradle:

    compile 't.cog.balok.balok.1.0-dev'

Maven:

    <dependency>
        <groupId>t.cog.balok</groupId>
        <artifactId>balok</artifactId>
        <version>1.0-dev</version>
    </dependency>

Remote Repostory
----------------

Gradle:

    repositories {
      maven {
          url "https://bitbucket.org/cogumbreiro/maven-repository/raw/master/"
      }
    }

Maven:

    <repositories>
        <repository>
            <id>tcog</id>
            <url>https://bitbucket.org/cogumbreiro/maven-repository/raw/master/</url>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>always</updatePolicy>
            </snapshots>
        </repository>
    </repositories>
