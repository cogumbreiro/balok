package balok.causality;

import java.util.Optional;

/**
 * Represents a causal event, parametrized by its epoch (a projection).
 *
 * @param <T> The type of the epoch.
 */
public interface Event<T> {
    /**
     * Projects this event into an epoch.
     * @return The epoch of the view.
     */
    T project();


    /**
     * Tests if the given epoch happened before this object.
     * @param epoch The other happened-before or is the current view.
     * @return
     */
    Optional<Causality> observe(T epoch);
}
