package balok.causality.async;

import balok.causality.Access;
import balok.causality.Event;
import com.carrotsearch.hppc.ObjectObjectHashMap;
import com.carrotsearch.hppc.ObjectObjectMap;
import com.carrotsearch.hppc.cursors.ObjectObjectCursor;
import balok.causality.AccessEntry;

import java.util.function.Supplier;

/**
 * The global shadow-memory. Takes frames and throws an exception when an data-race occurs.
 * @param <A> The type of the access (metadata)
 * @param <T> The type of the epoch.
 * @see Frame
 * @see FrameBuilder
 */
public final class ShadowMemory<A extends Access,T> {
    private final ObjectObjectMap<AsyncLocationTracker<A,T>, ShadowEntry<A,T>> accesses = new ObjectObjectHashMap<>();
    private final Supplier<ShadowEntry<A, T>> factory;

    public ShadowMemory() {
        this(DenseShadowEntry::new);
    }

    public ShadowMemory(Supplier<ShadowEntry<A, T>> factory) {
        this.factory = factory;
    }

    private ShadowEntry<A,T> get(AsyncLocationTracker loc) {
        int index = accesses.indexOf(loc);
        ShadowEntry<A, T> shadow;
        if (accesses.indexExists(index)) {
            shadow = accesses.indexGet(index);
        } else {
            shadow = factory.get();
            accesses.put(loc, shadow);
        }
        return shadow;
    }

    public void add(AsyncLocationTracker<A,T> location, AccessEntry<A,Event<T>> access, int ticket) {
        ShadowEntry<A, T> entry = get(location);
        entry.add(location, access, ticket);
        entry.refresh(location);
    }

    public void addAll(ShadowMemory<A, T> other) {
        for (ObjectObjectCursor<AsyncLocationTracker<A,T>, ShadowEntry<A,T>> cursor : other.accesses) {
            ShadowEntry<A, T> entry = get(cursor.key);
            entry.addAll(cursor.value);
            entry.refresh(cursor.key);
        }
    }

    public void clear() {
        accesses.clear();
    }

    @Override
    public String toString() {
        return accesses.toString();
    }
}
