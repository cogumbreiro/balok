package balok.causality.async;

import balok.causality.*;

import java.util.concurrent.atomic.AtomicInteger;

public class AsyncLocationTracker<A extends Access, T> extends LocationTracker<A, T> {
    public static final int TICKET_ZERO = Integer.MIN_VALUE;
    private final int hashCode;
    private static final AtomicInteger hashCodeGen = new AtomicInteger(Integer.MIN_VALUE);
    public AsyncLocationTracker() {
        this.hashCode = hashCodeGen.getAndIncrement();
    }

    private final AtomicInteger counter = new AtomicInteger(TICKET_ZERO);

    private final AtomicInteger nextTicket = new AtomicInteger(counter.get());

    public int createTicket() {
        int result = counter.getAndIncrement();
        assert result < Integer.MAX_VALUE;
        return result;
    }

    public boolean tryAdd(AccessEntry<A,Event<T>> access, int ticket) {
        if (ticket == nextTicket.get()) {
            if (lookupConflict(access) != null) {
                throw new IllegalStateException();
            } else {
                unsafeAdd(access);
            }
            nextTicket.getAndIncrement();
            return true;
        } else {
            return false;
        }
    }

    private void ensureReads(Iterable<AccessEntry<A, Event<T>>> entries) {
        for (AccessEntry<A, Event<T>> event : entries) {
            AccessEntry<A, T> readConflict = getReadConflict(event);
            if (readConflict != null) {
                throw new IllegalStateException();
            }
        }
    }

    private void ensureWrites(Iterable<AccessEntry<A, Event<T>>> entries) {
        for (AccessEntry<A, Event<T>> event : entries) {
            AccessEntry<A, T> writeConflict = getWriteConflict(event);
            if (writeConflict != null) {
                throw new IllegalStateException();
            }
        }
    }

    public boolean tryAdd(Segment<A, T> segment, Interval tickets) {
        if (tickets.getLowEndpoint() == nextTicket.get()) {
            ensureReads(segment.getPendingReads());
            ensureWrites(segment.getPendingWrites());
            for (AccessEntry<A, T> acc : segment.getWrites()) {
                write = acc;
            }
            this.read = new EpochSet<>();
            this.read.set(segment.getReads());
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return hashCode;
    }
}
