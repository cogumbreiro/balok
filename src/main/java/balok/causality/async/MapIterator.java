package balok.causality.async;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

/**
 * Used internally to operate on {@link Iterable}s functionally.
 * @param <T>
 * @param <U>
 */
final class MapIterator<T,U> implements Iterator<U> {
    private final Iterator<T> iter;

    private final Function<T,U> func;

    MapIterator(Iterator<T> accesses, Function<T,U> key) {
        this.iter = accesses;
        this.func = key;
    }

    @Override
    public boolean hasNext() {
        return iter.hasNext();
    }

    @Override
    public U next() {
        return func.apply(iter.next());
    }

    private static class MapIterable<T, U> implements Iterable<U> {
        private final Function<T,U> func;
        private final Iterable<T> elems;
        public MapIterable(Iterable<T> elems, Function<T,U> func) {
            this.elems = elems;
            this.func = func;
        }

        @Override
        public Iterator<U> iterator() {
            return new MapIterator<>(elems.iterator(), func);
        }
    }

    public static <T,U> Iterable<U> map(Iterable<T> elems, Function<T,U> func) {
        return new MapIterable<>(elems, func);
    }

    public static <T> List<T> asList(Iterable<T> elems) {
        ArrayList<T> result = new ArrayList<>();
        for (T elem : elems) {
            result.add(elem);
        }
        return result;
    }
}
