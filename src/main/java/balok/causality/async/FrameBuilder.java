package balok.causality.async;

import balok.causality.Access;
import balok.causality.Event;
import balok.causality.AccessEntry;

import java.util.Arrays;

public final class FrameBuilder<A extends Access, T> {
    private FrameEntry<A,T>[] entries;
    private int count;

    public FrameBuilder(int size) {
        this.entries = new FrameEntry[size];
    }

    public void add(AsyncLocationTracker<A,T> location, AccessEntry<A,Event<T>> access, int ticket) {
        entries[count++] = new FrameEntry<>(location, access, ticket);
    }

    public boolean isEmpty() {
        return count == 0;
    }

    public boolean isFull() {
        return entries.length == count;
    }

    public Frame<A, T> build() {
        Frame<A, T> frame = new Frame<>(entries, count);
        entries = new FrameEntry[entries.length];
        count = 0;
        return frame;
    }

    @Override
    public String toString() {
        return Arrays.toString(Arrays.copyOf(entries, count));
    }
}
