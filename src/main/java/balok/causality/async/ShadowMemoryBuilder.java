package balok.causality.async;

import balok.causality.Access;
import balok.causality.Event;
import balok.causality.AccessEntry;

public final class ShadowMemoryBuilder<A extends Access,T> {
    private ShadowMemory<A,T> buffer;
    private int size;
    private final int bufferSize;

    public ShadowMemoryBuilder(int bufferSize) {
        this.buffer = new ShadowMemory<>();
        this.bufferSize = bufferSize;
    }

    public void add(AsyncLocationTracker<A,T> location, AccessEntry<A,Event<T>> access, int ticket) {
        buffer.add(location, access, ticket);
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean isFull() { return size == bufferSize; }

    public ShadowMemory<A, T> build() {
        ShadowMemory<A, T> result = buffer;
        buffer = new ShadowMemory<>();
        size = 0;
        return result;
    }

    @Override
    public String toString() {
        return buffer.toString();
    }
}
