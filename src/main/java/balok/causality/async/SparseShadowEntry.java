package balok.causality.async;

import balok.causality.Access;
import balok.causality.Event;
import balok.causality.AccessEntry;

public class SparseShadowEntry<A extends Access,T> implements ShadowEntry<A,T> {
    private IntervalTree<Segment<A,T>> node = null;

    @Override
    public void add(AsyncLocationTracker<A,T> location, AccessEntry<A,Event<T>> access, int ticket) {
        add(Segment.makeInterval(Segment.make(access), new Interval(ticket)));
    }

    private void add(IntervalTree<Segment<A, T>> other) {
        if (node == null) {
            node = other;
        } else {
            node.add(other);
        }
    }

    @Override
    public void addAll(ShadowEntry<A, T> obj) {
        SparseShadowEntry<A, T> other = (SparseShadowEntry<A, T>) obj;
        add(other.node);
    }

    @Override
    public void refresh(AsyncLocationTracker<A, T> location) {
        IntervalTree<Segment<A,T>> entry = node;
        for (;entry != null; entry = node.getFirst()) {
            if (!location.tryAdd(entry.getValue(), entry.getInterval())) {
                return;
            }
            if (entry == node) {
                node = null;
                return;
            } else {
                node.remove(entry);
            }
        }
    }
}
