package balok.causality.async;

import balok.causality.Access;
import balok.causality.Event;
import balok.causality.AccessEntry;

interface ShadowEntry<A extends Access,T> {
    void add(AsyncLocationTracker<A,T> location, AccessEntry<A,Event<T>> access, int ticket);
    void refresh(AsyncLocationTracker<A,T> location);
    void addAll(ShadowEntry<A,T> other);
}
