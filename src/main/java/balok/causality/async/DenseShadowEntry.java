package balok.causality.async;

import balok.causality.Access;
import balok.causality.Event;
import balok.causality.AccessEntry;
import it.unimi.dsi.fastutil.ints.Int2ObjectAVLTreeMap;

public class DenseShadowEntry<A extends Access,T> implements ShadowEntry<A,T> {
    private Int2ObjectAVLTreeMap<AccessEntry<A,Event<T>>> enqueued = new Int2ObjectAVLTreeMap<>();

    @Override
    public void add(AsyncLocationTracker<A,T> location, AccessEntry<A,Event<T>> access, int ticket) {
        if (! location.tryAdd(access, ticket)) {
            enqueued.put(ticket, access);
        }
    }

    @Override
    public void refresh(AsyncLocationTracker<A,T> location) {
        if (enqueued.isEmpty()) {
            return;
        }
        int key = enqueued.firstIntKey();
        AccessEntry<A, Event<T>> entry = enqueued.get(key);
        for (;entry != null && location.tryAdd(enqueued.get(key), key); key = enqueued.firstIntKey(), entry = enqueued.get(key)) {
            enqueued.remove(key);
            if (enqueued.isEmpty()) {
                break;
            }
        }
    }

    @Override
    public void addAll(ShadowEntry<A, T> obj) {
        DenseShadowEntry<A, T> other = (DenseShadowEntry<A, T>) obj;
        enqueued.putAll(other.enqueued);
    }

    @Override
    public String toString() {
        return enqueued.toString();
    }
}
