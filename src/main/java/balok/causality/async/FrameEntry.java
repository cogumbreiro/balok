package balok.causality.async;

import balok.causality.Access;
import balok.causality.Event;
import balok.causality.AccessEntry;

/**
 * Internal class that pairs an access and some order-ticket.
 * @param <A> The type of the access (read/write + metadata)
 */
final class FrameEntry<A extends Access, T> {
    public final AsyncLocationTracker<A,T> location;
    public final AccessEntry<A,Event<T>> access;
    public final int ticket;

    public FrameEntry(AsyncLocationTracker<A,T> location, AccessEntry<A,Event<T>> access, int ticket) {
        this.access = access;
        this.ticket = ticket;
        this.location = location;
    }

    @Override
    public String toString() {
        return location + ":(" + new Interval(ticket) + "=>" + access + ")";
    }
}
