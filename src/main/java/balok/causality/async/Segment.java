package balok.causality.async;

import balok.causality.Access;
import balok.causality.Event;
import balok.causality.AccessEntry;

import java.util.function.Consumer;

public final class Segment<A extends Access, E> implements Consumer<Segment<A,E>> {
    private SegmentEntry<A, Event<E>> pending;
    private SegmentEntry<A, E> current;

    public Segment(SegmentEntry<A, Event<E>> pending, SegmentEntry<A, E> current) {
        this.pending = pending;
        this.current = current;
    }

    @Override
    public void accept(Segment<A, E> other) {
        this.current.check(other.pending);
        this.pending = this.pending.addPending(other.pending);
        this.current = this.current.addCurrent(other.current);
    }

    public Iterable<AccessEntry<A, Event<E>>> getPendingReads() {
        return pending.getReads();
    }

    public Iterable<AccessEntry<A, Event<E>>> getPendingWrites() {
        return pending.getWrites();
    }


    public Iterable<AccessEntry<A, E>> getReads() {
        return current.getReads();
    }

    public Iterable<AccessEntry<A, E>> getWrites() {
        return current.getWrites();
    }

    public static <A extends Access,E> Segment<A,E> make(AccessEntry<A,Event<E>> event) {
        AccessEntry<A, E> epoch = event.setValue(event.getValue().project());
        return new Segment<>(SegmentEntry.make(event), SegmentEntry.make(epoch));
    }

    public static <A extends Access,E> IntervalTree<Segment<A,E>> makeInterval(Segment<A,E> tracker, Interval range) {
        return new IntervalTree<>(tracker, range);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Segment<?, ?> that = (Segment<?, ?>) o;

        if (!pending.equals(that.pending)) return false;
        return current.equals(that.current);
    }

    public int size() {
        return current.size() + pending.size();
    }

    @Override
    public int hashCode() {
        int result = pending.hashCode();
        result = 31 * result + current.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "(pending=" + pending + ", current=" + current + ")";
    }
}
