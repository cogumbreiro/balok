package balok.causality.async;

import balok.causality.Access;

import java.util.Arrays;

/**
 * A Frame is a nonempty set of memory accesses, all associated to the same location.
 * @param <A> The type of the access (read/write) and metadata.
 * @param <T> The type of the epoch.
 * @see FrameBuilder
 */
public final class Frame<A extends Access,T> {
    private final FrameEntry<A, T>[] buffer;
    private final int size;

    public Frame(FrameEntry<A, T>[] buffer, int size) {
        this.buffer = buffer;
        this.size = size;
    }

    /**
     * Given a map,
     */
    public void addTo(ShadowMemory<A, T> mem) {
        for (int i = 0; i < size; i++) {
            FrameEntry<A, T> entry = buffer[i];
            mem.add(entry.location, entry.access, entry.ticket);
        }
    }

    @Override
    public String toString() {
        return Arrays.toString(Arrays.copyOf(buffer, size));
    }
}
