package balok.causality.async;

import balok.causality.Access;
import balok.causality.Event;
import com.carrotsearch.hppc.ObjectArrayList;
import com.carrotsearch.hppc.cursors.ObjectCursor;
import balok.causality.AccessEntry;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

abstract class SegmentEntry<A extends Access, E> {
    private SegmentEntry() {} // Disallow others of subclassing

    public abstract void check(SegmentEntry<A,Event<E>> other);
    public abstract SegmentEntry<A,E> addPending(SegmentEntry<A,E> other);
    public abstract SegmentEntry<A,E> addCurrent(SegmentEntry<A,E> other);
    public abstract Iterable<AccessEntry<A,E>> getReads();
    public abstract Iterable<AccessEntry<A,E>> getWrites();
    public abstract int size();

    private static <E> void ensureHappenedBefore(E left, Event<E> right) {
        if (! right.observe(left).isPresent()) {
            throw new IllegalStateException(left + " not HB " + right);
        }
    }
    private static <T> void addAll(ObjectArrayList<T> left, ObjectArrayList<T> right) {
        int offset = left.size();
        left.resize(offset + right.size());
        System.arraycopy(right.buffer, 0, left.buffer, offset, right.size());
    }

    public static <T> List<T> asList(ObjectArrayList<T> left) {
        return MapIterator.asList(MapIterator.map(left, x -> x.value));
    }

    private static final class RW<A extends Access, E> extends SegmentEntry<A,E> {
        public final AccessEntry<A,E> write;
        final ObjectArrayList<AccessEntry<A,E>> reads;

        RW(AccessEntry<A, E> write, ObjectArrayList<AccessEntry<A, E>> reads) {
            this.write = write;
            this.reads = reads;
        }

        @Override
        public void check(SegmentEntry<A, Event<E>> other) {
            E w_a = this.write.getValue();
            if (other instanceof RW) {
                RW<A, Event<E>> rw = (RW<A,Event<E>>) other;
                Event<E> b_W = rw.write.getValue();
                if (! b_W.observe(w_a).isPresent()) {
                    throw new IllegalStateException();
                }
                for (ObjectCursor<AccessEntry<A,E>> read : this.reads) {
                    SegmentEntry.ensureHappenedBefore(read.value.getValue(), b_W);
                }
            } else{
                RR<A,Event<E>> rr = (RR<A,Event<E>>) other;
                for (ObjectCursor<AccessEntry<A, Event<E>>> read : rr.reads) {
                    SegmentEntry.ensureHappenedBefore(w_a, read.value.getValue());
                }
            }
        }

        // (R_a,W_a),(r_a,w_a) + (R_b,W_b),(r_b,w_b)
        // =
        // (R_a,W_a),(r_b,w_b)

        // (R_a,W_a),(r_a,w_a) + R_b,r_b
        // =
        // (R_a,W_a),(r_a + r_b,w_a)
        @Override
        public SegmentEntry<A, E> addPending(SegmentEntry<A, E> other) {
            return this;
        }


        @Override
        public SegmentEntry<A, E> addCurrent(SegmentEntry<A, E> other) {
            if (other instanceof RW) {
                return other;
            } else {
                RR<A,E> rr = (RR<A, E>) other;
                addAll(this.reads, rr.reads);
                return this;
            }
        }

        @Override
        public Iterable<AccessEntry<A, E>> getReads() {
            return asList(reads);
        }

        @Override
        public Iterable<AccessEntry<A, E>> getWrites() {
            return Arrays.asList(write);
        }

        @Override
        public String toString() {
            return "(W=" + write + ", R=" + reads + ")";
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            RW<?, ?> rw = (RW<?, ?>) o;

            if (!write.equals(rw.write)) return false;
            return reads.equals(rw.reads);
        }

        @Override
        public int hashCode() {
            int result = write.hashCode();
            result = 31 * result + reads.hashCode();
            return result;
        }

        @Override
        public int size() {
            return 1 + reads.size();
        }
    }

    private static final class RR<A extends Access, E> extends SegmentEntry<A, E> {
        public final ObjectArrayList<AccessEntry<A,E>> reads;

        public RR(ObjectArrayList<AccessEntry<A,E>> reads) {
            this.reads = reads;
        }

        @Override
        public void check(SegmentEntry<A, Event<E>> other) {
            if (other instanceof RW) {
                RW<A, Event<E>> rw = (RW<A, Event<E>>) other;
                Event<E> W_b = rw.write.getValue();
                for (ObjectCursor<AccessEntry<A, E>> read : this.reads) {
                    SegmentEntry.ensureHappenedBefore(read.value.getValue(), W_b);
                }
            } else {
                // OK
            }
        }

        // R_a,r_a + (R_B,W_B),(r_b,w_b)
        // =
        // (R_A + R_B,W_B),(r_b,w_b)

        // R_a,r_a + R_b,r_b = R_a R_b,r_a r_b
        @Override
        public SegmentEntry<A, E> addPending(SegmentEntry<A, E> other) {
            if (other instanceof RW) {
                RW<A, E> rw = (RW<A, E>) other;
                addAll(this.reads, rw.reads);
                return new RW<>(rw.write, this.reads);
            } else {
                RR<A,E> rr = (RR<A, E>) other;
                addAll(this.reads, rr.reads);
                return this;
            }
        }

        @Override
        public SegmentEntry<A, E> addCurrent(SegmentEntry<A, E> other) {
            if (other instanceof RW) {
                return other;
            } else {
                RR<A,E> rr = (RR<A, E>) other;
                addAll(this.reads, rr.reads);
                return this;
            }
        }

        @Override
        public Iterable<AccessEntry<A, E>> getReads() {
            return asList(reads);
        }

        @Override
        public Iterable<AccessEntry<A, E>> getWrites() {
            return Collections.emptyList();
        }

        @Override
        public String toString() {
            return "(R=" + reads + ")";
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            RR<?, ?> rr = (RR<?, ?>) o;

            return reads.equals(rr.reads);
        }

        @Override
        public int size() {
            return reads.size();
        }

        @Override
        public int hashCode() {
            return reads.hashCode();
        }
    }

    public static <A extends Access, E> SegmentEntry<A, E> make(AccessEntry<A,E> access) {
        switch (access.getAccess().getMode()) {
            case READ:
                ObjectArrayList<AccessEntry<A, E>> singleton = new ObjectArrayList<>();
                singleton.add(access);
                return new RR<>(singleton);
            case WRITE:
                return new RW<>(access, new ObjectArrayList<>());
            default: throw new IllegalStateException();
        }
    }
}
