package balok.causality;

import gorn.util.Link;

/**
 * Represents a collection of events. Events can be merge together and compared
 * for reachability.
 */
public class EpochSet<A extends Access, T> {
	private static final int TRIES = 10;
    private Link<AccessEntry<A, T>> timestamps = null;

    public void set(Iterable<AccessEntry<A,T>> entries) {
        timestamps = null;
        for (AccessEntry<A,T> acc : entries) {
            timestamps = new Link<>(acc, timestamps);
        }
    }

    /**
     * Performs the union of both timestamps.
     * After merging any event that reached the other now reaches this as well.
     * @param other
     */
    public void add(AccessEntry<A, Event<T>> other) {
    	// upon adding an access, check if the same task is in the set:
	    // in which case we reuse that cell.
        // This search is bounded up to TRIES elements
        final Link<AccessEntry<A, T>> old = findPrecedes(other.getValue());
        final AccessEntry<A, T> epoch = other.setValue(other.getValue().project());
        if (old != null) {
            // We can safely mutate a set of epochs because these are only
            // ever mutated in one point, inside a lock
            old.element = epoch;
	    } else {
            timestamps = new Link<>(epoch, timestamps);
        }
    }

    /**
     * Tries to find a clock epoch that precedes this task view
     * @param other
     * @return Returns the element that contains it.
     */
    private Link<AccessEntry<A,T>> findPrecedes(Event<T> other) {
        int i = 0;
        // upon adding an access, check if the same task has read already
        // in which case when the same task has added an element, we maintain
        // only do so for the first TRIES elements
        for (Link<AccessEntry<A,T>> entry = timestamps; i < TRIES && entry != null; entry = entry.next, i++) {
            if (other.observe(entry.element.getValue()).isPresent()) {
                return entry;
            }
        }
        return null;
    }

    /**
     * Checks if a specific epoch has been added already
     * @param other
     * @return
     */
    public boolean contains(Event<T> other) {
    	for (AccessEntry<A,T> entry : Link.iterate(timestamps, TRIES)) {
            if (other.observe(entry.getValue()).isPresent()) {
			    return true;
		    }
	    }
	    return false;
    }

    public AccessEntry<A,T> lookupConflict(Event<T> other) {
        for (AccessEntry<A,T> elem : Link.iterate(timestamps)) {
            if (! other.observe(elem.getValue()).isPresent()) {
                return elem;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return Link.asList(timestamps).toString();
    }

}
