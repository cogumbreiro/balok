package balok.causality;

public enum AccessMode {
    READ,
    WRITE
}
