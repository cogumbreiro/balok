package balok.causality;

/**
 * An access entry stores an access and an event.
 * @param <A> The access type.
 * @param <E> The event type.
 */
public final class AccessEntry<A extends Access, E> {
    private final A access;
    private final E value;

    /**
     * The only constructor.
     * @param access
     * @param value
     */
    public AccessEntry(A access, E value) {
        this.access = access;
        this.value = value;
    }

    /**
     * Returns the access mode.
     * @return
     */
    public A getAccess() {
        return access;
    }

    /**
     * Returns the vent associated with this entry.
     * @return
     */
    public E getValue() {
        return value;
    }

    /**
     * Updates the value (event), returning the new entry.
     * @param newValue
     * @param <S>
     * @return
     */
    public <S> AccessEntry<A,S> setValue(S newValue) {
        return new AccessEntry<>(access, newValue);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccessEntry<?, ?> that = (AccessEntry<?, ?>) o;

        if (access != null ? !access.equals(that.access) : that.access != null) return false;
        return value != null ? value.equals(that.value) : that.value == null;
    }

    @Override
    public int hashCode() {
        int result = access != null ? access.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "(" + access + ", " + value + ")";
    }
}
