package balok.causality;

import balok.vc.CollectiveClock;

/**
 * Tracks barrier events of a specific barrier.
 */
public class PhaserTracker {
    protected final int id;
    protected final CollectiveClock globals;
    private final TimestampCache cache;

    protected PhaserTracker(int id, CollectiveClock phaseOrdering, TimestampCache cache) {
        this.id = id;
        this.globals = phaseOrdering;
        this.cache = cache;
    }

    /**
     * Advance the signal phase.
     */
    public void signal() {
        this.globals.signal(id);
        cache.invalidatePhaseOrdering();
    }

    /**
     * Advance the wait phase.
     */
    public void await() {
        this.globals.await(id);
        cache.invalidatePhaseOrdering();
    }

    /**
     * Advance the signal and the wait phase.
     */
    public void signalAwait() {
        this.globals.signalAwait(id);
        cache.invalidatePhaseOrdering();
    }

    @Override
    public String toString() {
        return id + "@" + globals;
    }
}
