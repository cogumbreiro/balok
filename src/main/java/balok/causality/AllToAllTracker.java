package balok.causality;

import balok.vc.AllToAllOrdering;

/**
 * Tracks barrier events of a specific barrier.
 */
public class AllToAllTracker {
    protected final int id;
    protected final AllToAllOrdering phases;

    protected AllToAllTracker(int id, AllToAllOrdering phases) {
        this.id = id;
        this.phases = phases;
    }

    /**
     * Advance the signal and the wait phase.
     */
    public void signalAwait() {
        this.phases.awaitAdvance(id);
    }

    @Override
    public String toString() {
        return id + "@" + phases;
    }
}
