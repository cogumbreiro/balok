package balok.causality;

import java.util.Optional;

public enum Causality {
    HAPPENED_AFTER,
    EQ,
    ;
    public static final Optional<Causality> SOME_HAPPENED_AFTER = Optional.of(Causality.HAPPENED_AFTER);
    public static final Optional<Causality> SOME_EQ = Optional.of(Causality.EQ);
    public static final Optional<Causality> NONE = Optional.empty();
}
