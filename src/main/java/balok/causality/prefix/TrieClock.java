package balok.causality.prefix;

import balok.causality.Causality;
import balok.causality.ClockController;
import balok.causality.Event;

import java.util.Optional;

public class TrieClock implements ClockController<SpawnPath>, Event<SpawnPath> {
    private SpawnPath path;
    private Trie joins;

    public TrieClock() {
        this(new SpawnPath(), null);
    }

    public TrieClock(SpawnPath path, Trie joins) {
        this.path = path;
        this.joins = joins;
    }

    public boolean contains(SpawnPath other) {
        return path.contains(other) || (joins != null && joins.contains(other));
    }

    public SpawnPath getPath() {
        return path;
    }

    @Override
    public TrieClock spawnChild() {
        SpawnPath newPath = path.createChild();
        return new TrieClock(newPath, joins);
    }

    @Override
    public TrieClock join(Event<SpawnPath> view) {
        TrieClock other = (TrieClock) view;
        if (joins != null && joins.contains(other.path)) {
            // avoid merging if already contained
            return this;
        }
        Trie newJoins = Trie.merge(joins, other.joins);
        if (newJoins == null) {
            newJoins = new Trie();
        }
        newJoins = newJoins.add(other.path);
        SpawnPath newPath = path.clone();
        newPath.advance();
        return new TrieClock(newPath, newJoins);
    }

    @Override
    public TrieClock produceEvent() {
        SpawnPath newPath = path.clone();
        newPath.advance();
        return new TrieClock(newPath, joins);
    }

    @Override
    public Event<SpawnPath> createView() {
        return this;
    }

    @Override
    public SpawnPath project() {
        return path;
    }

    @Override
    public Optional<Causality> observe(SpawnPath epoch) {
        if (path.weakEquals(epoch)) { return Optional.of(Causality.EQ); }
        return contains(epoch) ? Optional.of(Causality.HAPPENED_AFTER) : Optional.empty();
    }


    @Override
    public String toString() {
        return "{\"id\":" + path + (joins == null ? "" : ",\"j\":" + SpawnPath.asList(joins.asList())) + "}";
    }
}
