package balok.causality.prefix;

import com.carrotsearch.hppc.ObjectStack;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Represents a set of {@link SpawnPath} objects.
 * This data structure is akin to a vector clock.
 * Adding a {@link SpawnPath} is equivalent to adding an
 * element to a set. If there's a member that happens-before
 * an added element, then we delete the said member. The
 * invariant is that the set consists of concurrent spawn-paths.
 */
public class TreePrefixClock implements PrefixClock {
    private TreePrefixClock small;
    private TreePrefixClock large;
    private SpawnPath value;

    /**
     * Creates an empty set.
     */
    public TreePrefixClock() {}

    /**
     * Creates a singleton set.
     * @param path
     */
    private TreePrefixClock(SpawnPath path) {
        this.value = path;
    }

    /**
     * Creates a shallow copy of this set. This is useful for
     * implementing immutable operations (see persistent data structures).
     * @return The shallow copy.
     */
    private TreePrefixClock shallowCopy() {
        TreePrefixClock result = new TreePrefixClock(value);
        result.small = small;
        result.large = large;
        return result;
    }

    /**
     * Returns a member that happens-before or -after the given
     * path.
     * @param target
     * @param tid
     * @return The matched object; null if nothing is found.
     */
    private static Order match(TreePrefixClock target, SpawnPath tid) {
        while (target != null) {
            Order order = tid.match(target.value);
            if (order.reflexiveOrdered()) {
                return order;
            }
            if (order == Order.GT) {
                target = target.small;
            } else {
                target = target.large;
            }
        }
        return null;
    }

	@Override
    public boolean contains(SpawnPath tid) {
        if (value == null) {
            return false;
        }
	    Order order = TreePrefixClock.match(this, tid);
	    return order != null ? order.reflexiveHappensBefore() : false;
    }

    private static void insert(TreePrefixClock tree, SpawnPath tid) {
        for (;;) {
            Order result = tree.value.match(tid);
            if (result.reflexiveOrdered()) {
                // overlap
                if (result.happensBefore()) {
                    // we have an outdated value; update
                    tree.value = tid;
                }
                return;
            }
            if (result == Order.LT) {
                if (tree.small == null) {
                    tree.small = new TreePrefixClock(tid);
                    return;
                } else {
                    tree = tree.small;
                }
            } else { // result == order.GT
                if (tree.large == null) {
                    tree.large = new TreePrefixClock(tid);
                    return;
                } else {
                    tree = tree.large;
                }
            }
        }
    }

    private static TreePrefixClock immutableInsert(TreePrefixClock tree, SpawnPath tid) {
        final TreePrefixClock root = tree.shallowCopy();
        tree = root;
        for (;;) {
            Order result = tree.value.match(tid);
            if (result.reflexiveOrdered()) {
                // overlap
                if (result.happensBefore()) {
                    // we have an outdated value; update
                    tree.value = tid;
                }
                break;
            }
            if (result == Order.LT) {
                if (tree.small == null) {
                    tree.small = new TreePrefixClock(tid);
                    break;
                } else {
                    tree = tree.small = tree.small.shallowCopy();
                }
            } else { // result == order.GT
                if (tree.large == null) {
                    tree.large = new TreePrefixClock(tid);
                    break;
                } else {
                    tree = tree.large = tree.large.shallowCopy();
                }
            }
        }
        return root;
    }

    @Override
	public void add(SpawnPath tid) {
        if (value == null) {
            this.value = tid;
            return;
        }
        TreePrefixClock.insert(this, tid);
    }

    @Override
    public TreePrefixClock immutableAdd(SpawnPath tid) {
	    if (contains(tid)) {
    		// avoid creating new objects if the path is already in the set
    		return this;
	    }
    	return TreePrefixClock.immutableInsert(this, tid);
    }

	private void indent(StringBuilder buffer, int depth) {
        for (int i = 0; i < depth; i++) {
            buffer.append(' ');
        }
    }

    private void toString(StringBuilder buffer, int depth) {
        indent(buffer, depth);
        if (value == null) {
            buffer.append("NULL");
        } else {
            buffer.append(value.toString());
        }
        if (small != null) {
            indent(buffer, depth + 1);
            buffer.append("small:");
            small.toString(buffer, depth + 1);
        }
        if (large != null) {
            indent(buffer, depth + 1);
            buffer.append("large:");
            large.toString(buffer, depth + 1);
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        toString(result, 0);
        return result.toString();
    }

    private static List<SpawnPath> asList(TreePrefixClock set) {
        ArrayList<SpawnPath> result = new ArrayList<>();
        ObjectStack<TreePrefixClock> toProcess = new ObjectStack<>();
        if (set.value != null) {
            toProcess.push(set);
        }
        while (toProcess.size() > 0) {
            TreePrefixClock elem = toProcess.pop();
            result.add(elem.value);
            if (elem.small != null) toProcess.push(elem.small);
            if (elem.large != null) toProcess.push(elem.large);
        }
        return result;
    }

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof PrefixClock)) {
		    return false;
        }
        return equalsTo((PrefixClock) o);
	}

	@Override
	public int hashCode() {
		int result = small != null ? small.hashCode() : 0;
		result = 31 * result + (large != null ? large.hashCode() : 0);
		result = 31 * result + value.hashCode();
		return result;
	}

	@Override
    public Iterator<SpawnPath> iterator() {
        return asList(this).iterator();
    }
}
