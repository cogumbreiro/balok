package balok.causality.prefix;

/**
 * @autor Tiago Cogumbreiro
 */
public enum Order {
    LT,
    GT,
    EQ,
    HAPPENS_BEFORE,
    HAPPENS_AFTER;

    public final boolean isOrdered() {
        switch (this) {
            case HAPPENS_AFTER:
            case HAPPENS_BEFORE:
                return true;
            default:
                return false;
        }
    }

    public final boolean reflexiveOrdered() {
        switch (this) {
            case EQ:
            case HAPPENS_AFTER:
            case HAPPENS_BEFORE:
                return true;
            default:
                return false;
        }
    }

    /**
     * Happens before.
     * @return
     */
    public final boolean happensBefore() {
        switch (this) {
            case HAPPENS_BEFORE:
                return true;
            default:
                return false;
        }
    }

    public final boolean reflexiveHappensBefore() {
        switch (this) {
            case HAPPENS_BEFORE:
            case EQ:
                return true;
            default:
                return false;
        }
    }

    public final int compare() {
        switch (this) {
            case GT:
                return 1;
            case LT:
                return -1;
            default:
                return 0;
        }
    }

    public final boolean isConcurrent() {
        switch (this) {
            case GT:
            case LT:
                return true;
            default:
                return false;
        }
    }

    public final Order symmetric() {
        switch (this) {
            case GT:
                return LT;
            case LT:
                return GT;
            case HAPPENS_BEFORE:
                return HAPPENS_AFTER;
            case HAPPENS_AFTER:
                return HAPPENS_BEFORE;
            default:
                return this;
        }
    }
}
