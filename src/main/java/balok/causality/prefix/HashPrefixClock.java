package balok.causality.prefix;

import com.carrotsearch.hppc.IntObjectHashMap;
import com.carrotsearch.hppc.cursors.IntObjectCursor;

import java.util.Iterator;

public class HashPrefixClock implements PrefixClock {
	private final IntObjectHashMap<TreePrefixClock> entries;
	public HashPrefixClock() {
		entries = new IntObjectHashMap<>();
	}

	protected HashPrefixClock(IntObjectHashMap<TreePrefixClock> entries) {
		this.entries = entries;
	}


	public void add(SpawnPath path) {
		int key = path.id();
		int index = entries.indexOf(key);
		if (entries.indexExists(index)) {
			TreePrefixClock spawnPaths = entries.indexGet(index);
			entries.indexReplace(index, spawnPaths.immutableAdd(path));
		} else {
			TreePrefixClock set = new TreePrefixClock();
			set.add(path);
			entries.indexInsert(index, key, set);
		}
	}

    @Override
    public PrefixClock immutableAdd(SpawnPath tid) {
	    PrefixClock result = clone();
	    result.add(tid);
        return result;
    }

    @Override
	public HashPrefixClock clone() {
		return new HashPrefixClock(entries.clone());
	}

	public void merge(PrefixClock clock) {
	    if (clock instanceof HashPrefixClock) {
	        HashPrefixClock other = (HashPrefixClock) clock;

            for (IntObjectCursor<TreePrefixClock> cursor : other.entries) {
                int index = entries.indexOf(cursor.key);
                if (entries.indexExists(index)) {
                    TreePrefixClock spawnPaths = entries.indexGet(index);
                    for (SpawnPath path : cursor.value) {
                        spawnPaths = spawnPaths.immutableAdd(path);
                    }
                    entries.indexReplace(index, spawnPaths);
                } else {
                    entries.indexInsert(index, cursor.key, cursor.value);
                }
            }
        } else {
	        PrefixClock.super.merge(clock);
        }
    }

	public boolean contains(SpawnPath tid) {
		int index = entries.indexOf(tid.id());
		if (! entries.indexExists(index)) {
			return false;
		}
		TreePrefixClock spawnPaths = entries.indexGet(index);
		return spawnPaths.contains(tid);
	}

	private TreePrefixClock asSet() {
		TreePrefixClock set = new TreePrefixClock();
		for (IntObjectCursor<TreePrefixClock> cursor : entries) {
			for (SpawnPath path : cursor.value) {
				set.add(path);
			}
		}
		return set;
	}

	@Override
	public Iterator<SpawnPath> iterator() {
		return asSet().iterator();
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PrefixClock)) {
            return false;
        }
        return equalsTo((PrefixClock) o);
    }

	@Override
	public String toString() {
		return SpawnPath.asList(this).toString();
	}
}

