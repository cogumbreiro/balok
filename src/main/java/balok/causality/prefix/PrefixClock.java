package balok.causality.prefix;

public interface PrefixClock extends Iterable<SpawnPath>, Cloneable {

    /**
     * Finds the prefix that contains the given spawn path.
     * @param tid
     * @return
     */
    default SpawnPath find(SpawnPath tid) {
        for (SpawnPath path : this) {
            if (path.contains(tid)) {
                return path;
            }
        }
        return null;
    }

    /**
     * Checks if the prefix clock contains the given id.
     * @param tid
     * @return
     */
    boolean contains(SpawnPath tid);

    /**
     * Adds the spawn path to the current set (mutable).
     * @param tid
     */
    void add(SpawnPath tid);

    /**
     * Immutable version of adding a path to self.
     * @param tid
     * @return Returns this if no change was done.
     */
    PrefixClock immutableAdd(SpawnPath tid);

    /**
     * Merge this prefix clock with the target clock.
     * @param other
     */
    default void merge(PrefixClock other) {
        for (SpawnPath path : other) {
            add(path);
        }
    }

    /**
     * Immutable version of merging two clocks together.
     * @param other
     * @return
     */
    default PrefixClock immutableMerge(PrefixClock other) {
        PrefixClock result = this;
        for (SpawnPath path : other) {
            result = result.immutableAdd(path);
        }
        return result;
    }

    default boolean equalsTo(PrefixClock other) {
        if (other == null) {
            return false;
        }
        return SpawnPath.asList(this).equals(SpawnPath.asList(other));
    }
}
