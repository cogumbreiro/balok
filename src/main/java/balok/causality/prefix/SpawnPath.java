package balok.causality.prefix;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Represents a spawnpath.
 *
 * @author Tiago Cogumbreiro
 */
public class SpawnPath implements Comparable<SpawnPath>, Cloneable {
	private final int[] path;
	private int time;

	/**
	 * By default creates the root object [0].
	 */
	public SpawnPath() {
		this(new int[]{0}, 0);
	}

	/**
	 * Creates a spawn path given the array of the path itself.
	 *
	 * @param components
	 */
	public SpawnPath(int[] components, int time) {
		this.path = components;
		this.time = time;
	}
	/**
	 * Returns the index of the first value (left-to-right) that
	 * differs in both arrays.
	 *
	 * @param left
	 * @param right
	 * @return
	 */
	private static int commonPrefix(int[] left, int[] right) {
		int prefix;
		final int minLen = Math.min(left.length, right.length);
		for (prefix = 0; prefix < minLen && left[prefix] == right[prefix]; prefix++) {
			// nothing to do
		}
		return prefix;
	}

	/**
	 * Conditions for happens before, assuming there is a common prefix up to the given index
	 * <code>prefix</code>.
	 *
	 * @param prefix
	 * @param other
	 * @return
	 */
	private boolean happensBefore(int prefix, SpawnPath other) {
		if (this.path.length > other.path.length) {
			return false;
		}
		if (prefix == other.path.length) {
			return this.time < other.time;
		}
		if (prefix != this.path.length) {
			return false;
		}
		return this.time < other.path[prefix] || (this.time == other.path[prefix] && other.path.length > prefix);
	}

	private static Order matchTime(int leftTime, int rightTime) {
		return leftTime == rightTime ? Order.EQ :
				(leftTime < rightTime ? Order.HAPPENS_BEFORE : Order.HAPPENS_AFTER);
	}

	/**
	 * Compares both spawn paths. The two spawn paths can be either:
	 * <ul>
	 * <li>equal (point-wise)</li>
	 * <li>observe with respect to the happens-before relation</li>
	 * <li>unordered wrt the happens-before (aka parallel), in which
	 * case they can be either LT or GT, observe by lexicographically.</li>
	 * </ul>
	 *
	 * @param other
	 * @return
	 */
	public Order match(SpawnPath other) {
		if (other == this) {
			return Order.EQ;
		}
		if (this.path == other.path) {
			return matchTime(this.time, other.time);
		}
		// returns the common prefix between this and the other
		int prefix = commonPrefix(this.path, other.path);

		// check if these are the same
		if (prefix == this.path.length && prefix == other.path.length) {
			return matchTime(this.time, other.time);
		}

		// direct child
		if (happensBefore(prefix, other)) {
			return Order.HAPPENS_BEFORE;
		}

		// direct child
		if (other.happensBefore(prefix, this)) {
			return Order.HAPPENS_AFTER;
		}

		// ancestor's child
		if (prefix == this.path.length) {
			return time < other.path[prefix] ? Order.LT : Order.GT;
		}
		if (prefix == other.path.length) {
			return this.path[prefix] < other.time ? Order.LT : Order.GT;
		}
		return this.path[prefix] < other.path[prefix] ? Order.LT : Order.GT;
	}

	/**
	 * Returns the backing array.
	 *
	 * @return
	 */
	public int[] getId() {
		return path;
	}

	/**
	 * Returns the local timestamp of this task.
	 *
	 * @return
	 */
	public int getTime() {
		return time;
	}

	/**
	 * The 0-th position is the current event count, -1 is for the parent's event count and so on.
	 *
	 * @param offset
	 * @return
	 */
	public int get(int offset) {
		return offset == 0 ? time : path[path.length - offset];
	}

	/**
	 * Advances the local timestamp of this task.
	 */
	public void advance() {
		time++;
	}

	/**
	 * Creating a child augments the size of the array.
	 * The child is the same as the parent with a zero appended.
	 *
	 * @return
	 */
	public SpawnPath createChild() {
		int[] newBuffer = Arrays.copyOf(path, path.length + 1);
		newBuffer[path.length] = time;
		return new SpawnPath(newBuffer, 0);
	}

	/**
	 * Given two paths returns the one that is lexicographically smaller.
	 * In case of matching (observe) return the left.
	 *
	 * @param left
	 * @param right
	 * @return
	 */
	public static SpawnPath max(SpawnPath left, SpawnPath right) {
		return left.compareTo(right) >= 0 ? left : right;
	}

	/**
	 * Given two paths returns the one that is lexicographically greater.
	 * In case of matching (observe) return the left.
	 *
	 * @param left
	 * @param right
	 * @return
	 */
	public static SpawnPath min(SpawnPath left, SpawnPath right) {
		return left.compareTo(right) <= 0 ? left : right;
	}

	/**
	 * Shows the backing array.
	 *
	 * @return
	 */
	@Override
	public String toString() {
        int[] result = Arrays.copyOf(this.path, this.path.length + 1);
        result[path.length] = time;
        return Arrays.toString(result);
	}

	/**
	 * Built a path from an array.
	 * The returned object now owns the array.
	 *
	 * @param tid
	 * @return
	 */
	public static SpawnPath fromArray(int... tid) {
		int[] path = Arrays.copyOf(tid, tid.length - 1);
		return new SpawnPath(path, tid[tid.length - 1]);
	}

	/**
	 * This returns the lexicographic order, not the happens-before ordering.
	 *
	 * @param other
	 * @return
	 */
	@Override
	public int compareTo(SpawnPath other) {
		return match(other).compare();
	}

	public boolean isParentOf(SpawnPath other) {
		return this.path.length + 1 == other.path.length
				&& commonPrefix(this.path, other.path) == this.path.length;
	}

	/**
	 * Checks if the target path is a previous version (or equals) the current path.
	 *
	 * @param other
	 * @return
	 */
	public boolean contains(SpawnPath other) {
		return other.match(this).reflexiveHappensBefore();
	}

	/**
	 * Returns a copy of the path. This is an O(n) operation on the size of the path.
	 *
	 * @return
	 */
	@Override
	public SpawnPath clone() {
		return new SpawnPath(path, time);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		SpawnPath that = (SpawnPath) o;
		if (time != that.time) return false;
		return path == that.path || Arrays.equals(path, that.path);
	}

    /**
     * In the case the spawn paths are all created using {{@link SpawnPath#createChild()}},
     * {{@link SpawnPath#advance()}}, and {{@link SpawnPath#clone()}}, then we can check
     * if two tasks are equal, just by comparing the references.
     * @param other
     * @return
     */
	public boolean weakEquals(SpawnPath other) {
	    return this == other || (other != null && this.time == other.time && this.path == other.path);
    }

	private static int id(int[] array, int offset) {
		// Taken from com.carrotsearch.hppc.BitSet#hashCode()
		// Start with a zero hash and use a mix that results in zero if the input is zero.
		// This effectively truncates trailing zeros without an explicit check.
		long h = 0;
		for (int i = array.length - offset; --i >= 0;)
		{
			h ^= array[i];
			h = (h << 1) | (h >>> 63); // rotate left
		}

		// fold leftmost bits into right and add a constant to prevent
		// empty sets from returning 0, which is too common.
		return (int) ((h >> 32) ^ h) + 0x98761234;
	}

	/**
	 * Computes a unique id for the spawn-path.
	 * @return
	 */
	public int id() {
		return id(path, 0);
	}

	@Override
	public int hashCode() {
		int result = Arrays.hashCode(path);
		result = 31 * result + time;
		return result;
	}

    private static void add(List<SpawnPath> list, SpawnPath right) {
        Iterator<SpawnPath> iter = list.iterator();
        int idx = 0;
        while (iter.hasNext()) {
            SpawnPath left = iter.next();
            Order match = left.match(right);
            if (match.compare() > 0) {
                list.add(idx, right);
                return;
            } else if (match.compare() == 0) {
                list.set(idx, match.happensBefore() ? right : left);
                return;
            } else {
                idx++;
            }
        }
        list.add(idx, right);
    }

    public static List<SpawnPath> asList(Iterable<SpawnPath> elems) {
        List<SpawnPath> list = new ArrayList<>();
        for (SpawnPath path : elems) {
            add(list, path);
        }
        return list;
    }

}
