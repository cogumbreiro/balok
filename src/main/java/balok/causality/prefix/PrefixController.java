package balok.causality.prefix;

import balok.causality.Causality;
import balok.causality.ClockController;
import balok.causality.Event;
import com.carrotsearch.hppc.BitSet;
import com.carrotsearch.hppc.ObjectStack;
import gorn.util.Link;

import java.util.Optional;

public class PrefixController implements ClockController<SpawnPath>, Event<SpawnPath> {
    private final SpawnPath value;
    private final SpawnPath min;
    private final SpawnPath max;
    private final Link<PrefixController> joins;
    private final BitSet children;

    public PrefixController() {
        this(new SpawnPath());
    }

    public PrefixController(SpawnPath path) {
        this(path, path, path, null, null);
    }

    protected PrefixController(SpawnPath value, SpawnPath min, SpawnPath max, Link<PrefixController> joins, BitSet children) {
        this.value = value;
        this.min = min;
        this.max = max;
        this.joins = joins;
        this.children = children;
    }

    private boolean withinRange(SpawnPath other) {
        return other.compareTo(value) == 0 || (other.compareTo(min) >= 0 && other.compareTo(max) <= 0);
    }

    private static boolean happensBefore(SpawnPath left, PrefixController right, int cutOff) {
        if (! right.withinRange(left)) {
            return false;
        }
        if (left.match(right.value).reflexiveHappensBefore()
                || right.childJoinedWith(left)) {
            return true;
        }
        if (right.joins == null) {
            return false;
        }
        ObjectStack<PrefixController> toProcess = new ObjectStack<>();
        for (PrefixController entry : Link.iterate(right.joins)) {
            toProcess.add(entry);
        }
        int checks = 1;
        HashPrefixClock visited = new HashPrefixClock(); // <-- source of slowdown
        visited.add(right.value);
        visited.add(left);
        while (toProcess.size() > 0) {
            PrefixController elem = toProcess.pop();
            if (! elem.withinRange(left)) {
                continue;
            }
            if (left.match(elem.value).reflexiveHappensBefore()
                    || elem.childJoinedWith(left)) {
                return true;
            }
	        for (PrefixController entry : Link.iterate(elem.joins)) {
                // % 3 improves performance by 2x
                if (!visited.contains(entry.value) && checks % 3 == 0) { // <---- hotspot
                    toProcess.add(entry);
                    visited.add(entry.value); // <--- hotspot
                }
	        }
	        checks++;
            if (cutOff >= 0 && checks >= cutOff) {
            	return false;
            }
	        if (checks > 20000) {
                // DEBUGGING INFO
                System.out.println("CUTOFF=" + cutOff);
                System.err.println("CHECKS=" + checks);
                System.err.println(left + " HB " + right.value);
                System.err.println("REMAINING ELMES TO PROCESS=" + toProcess.size());
                System.err.println("STOPPED AT: " + elem);
                System.err.println(visited);
		        System.exit(-1);
	        }
        }
        return false;
    }
    public static boolean happensBefore(SpawnPath left, PrefixController right) {
        return happensBefore(left, right, -1);
    }

    private void printTab(int index) {
        for (int i = 0; i < index; i++) {
            System.err.print(" ");
        }
    }

    public void printDot(int index) {
        boolean didPrint = false;
    	for (PrefixController entry : Link.iterate(joins)) {
    	    printTab(index);
    		System.err.println("\"" + value + "\" -> \"" + entry.value + "\"");
    		didPrint = true;
	    }
	    if (didPrint) {
    	    System.out.println("");
        }
        for (PrefixController entry : Link.iterate(joins)) {
            entry.printDot(index + 1);
        }
    }

	@Override
	public boolean equals(Object ct) {
    	if (ct instanceof PrefixController) {
    		return false;
	    }
		PrefixController other = (PrefixController) ct;
		return other.value.equals(other.value);
	}

	private static final int TRIES = 100;

	private BitSet createNewChildren(PrefixController other) {
        BitSet newChildren = children != null ?
                (BitSet) children.clone() : null;
        if (value.isParentOf(other.value)) {
            // protect against multi-joins with children
            if (newChildren == null) {
                newChildren = new BitSet();
            }
            // mark child as joined
            newChildren.set(other.value.get(1));
        }
	    return newChildren;
    }

    private Link<PrefixController> createNewJoins(PrefixController other) {
        // Replace an already existing element
        int idx = 0;
        Link<PrefixController> newJoins = null;
        for (PrefixController ctrl : Link.iterate(joins, TRIES)) {
            if (ctrl.value.match(other.value).happensBefore()) {
                return joins.set(idx, other);
            }
            idx++;
        }
        return new Link<>(other, this.joins);
    }

    private boolean childJoinedWith(SpawnPath other) {
        return children != null && value.isParentOf(other) && children.get(other.get(1));
    }

    @Override
    public PrefixController join(Event<SpawnPath> c) {
        PrefixController other = (PrefixController) c;
        // 1. check children joined multiple times
	    if (childJoinedWith(other.value)) {
        	return this;
	    }
        // 2. check target against last added
        if (happensBefore(other.value, this, TRIES)) {
            return this;
        }
        // advance the current path
        SpawnPath newValue = value.clone();
        newValue.advance();

        BitSet newChildren = createNewChildren(other);
        Link<PrefixController> newJoins = createNewJoins(other);

        return new PrefixController(
                newValue,
                SpawnPath.min(this.min, other.min),
                SpawnPath.max(this.max, other.max),
                newJoins,
                newChildren
        );
    }

    @Override
    public ClockController spawnChild() {
        SpawnPath newVal = value.createChild();
        SpawnPath newMin = min == value ? newVal : min;
        SpawnPath newMax = max == value ? newVal : max;
        return new PrefixController(newVal, newMin, newMax, joins, null);
    }

    @Override
    public PrefixController produceEvent() {
        // advance the current path
        SpawnPath newVal = value.clone();
        newVal.advance();
        return new PrefixController(
                newVal,
                min == value ? newVal : min,
                max == value ? newVal : max,
                joins,
                children
        );
    }

    @Override
    public SpawnPath project() {
        return value;
    }

    @Override
    public PrefixController createView() {
        return this;
    }

    @Override
    public String toString() {
        return "{\"id\":" + value + (joins == null ? "" : ",\"j\":" + joins) + "}";
    }

    @Override
    public Optional<Causality> observe(SpawnPath epoch) {
	    if (epoch == value) {
	        return Optional.of(Causality.EQ);
        }
        return happensBefore(epoch, this) ? Optional.of(Causality.HAPPENED_AFTER) : Optional.empty();
    }

    public SpawnPath getValue() {
        return value;
    }
}

