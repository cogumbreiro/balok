package balok.causality.prefix;

import com.carrotsearch.hppc.IntObjectHashMap;
import com.carrotsearch.hppc.cursors.IntObjectCursor;

import java.util.ArrayList;
import java.util.List;

public class Trie {
    final int value;
    final IntObjectHashMap<Trie> children;

    public Trie(int value, IntObjectHashMap<Trie> children) {
        this.value = value;
        this.children = children;
    }

    public Trie() {
        this(0, null);
    }

    private Trie setValue(int newValue) {
        newValue = Math.max(value, newValue);
        return newValue == value ? this : new Trie(newValue, children);
    }

    private Trie setChild(int index, Trie child) {
        IntObjectHashMap<Trie> newChildren = children == null ? new IntObjectHashMap<>() : children.clone();
        newChildren.put(index, child);
        return new Trie(Math.max(value, index), newChildren);
    }

    private static Trie createPath(int[] key, int offset, int value) {
        Trie result = new Trie(value, null);
        for (int idx = key.length - 1; idx >= offset; idx--) {
            Trie newTrie = new Trie(0, new IntObjectHashMap<>());
            newTrie.children.put(idx, result);
            result = newTrie;
        }
        return result;
    }

    private Trie insert(int offset, int[] key, int value) {
        if (offset >= key.length) {
            // base case, set the value of the node
            return setValue(value);
        }
        int idx = key[offset];
        Trie child = children != null ? children.get(idx) : null;
        return setChild(idx, child != null
                ? child.insert(offset + 1, key, value)
                : createPath(key, offset + 1, value));
    }

    public Trie add(SpawnPath path) {
        return insert(0, path.getId(), path.getTime());
    }

    private static Trie lookup(Trie trie, int[] key) {
        for (int i = 0; i < key.length && trie != null; i++) {
            trie = trie.children.get(key[i]);
        }
        return trie;
    }

    private int lookup(int[] key, int offset) {
        if (offset >= key.length) {
            return value;
        }
        int idx = key[offset];
        Trie child = children.get(idx);
        return child != null ? lookup(key, offset + 1) : 0;
    }

    public boolean contains(SpawnPath path) {
        Trie node = lookup(this, path.getId());
        return node != null ? node.value >= path.getTime() : false;
    }

    public static Trie merge(Trie left, Trie right) {
        if (left == null) {
            return right;
        }
        if (right == null) {
            return left;
        }

        int newValue = Math.max(left.value, right.value);
        IntObjectHashMap<Trie> newChildren;
        if (left.children == null) {
            newChildren = right.children;
        } else if (right.children == null) {
            newChildren = left.children;
        } else {
            newChildren = new IntObjectHashMap<>();
            for (IntObjectCursor<Trie> cursor : left.children) {
                // merge left with right
                newChildren.put(cursor.key, merge(cursor.value, right.children.get(cursor.key)));
            }
            // add values that are not in left
            for (IntObjectCursor<Trie> cursor : right.children) {
                if (! left.children.containsKey(cursor.key)) {
                    newChildren.put(cursor.key, cursor.value);
                }
            }
        }
        return new Trie(newValue, newChildren);
    }

    public Trie merge(Trie other) {
        return Trie.merge(this, other);
    }

    // -----

    private static SpawnPath create(List<Integer> key, int time) {
        Integer[] tmp = key.toArray(new Integer[0]);
        int[] result = new int[tmp.length];
        for (int i = 0; i < tmp.length; i++) {
            result[i] = tmp[i];
        }
        return new SpawnPath(result, time);
    }

    private void fillList(List<SpawnPath> elems, List<Integer> path) {
        elems.add(create(path, this.value));
        if (children == null) {
            return;
        }
        path.add(0);
        int idx = path.size() - 1;
        for (IntObjectCursor<Trie> cursor : children) {
            path.set(idx, cursor.key);
            cursor.value.fillList(elems, path);
        }
        path.remove(idx); // remove object as it is no longer needed
    }

    public List<SpawnPath> asList() {
        ArrayList<SpawnPath> result = new ArrayList<>();
        fillList(result, new ArrayList<>());
        return SpawnPath.asList(result);
    }

    @Override
    public String toString() {
        return "{\"v\": " + value + ", \"j\": " + children + "}";
    }
}
