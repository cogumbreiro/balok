package balok.causality;

import balok.causality.vc.FlatController;
import balok.causality.vc.MutableVector;
import balok.causality.vc.PersistentVector;
import balok.causality.vc.StackedController;
import balok.causality.prefix.PrefixController;

public enum PtpCausalityFactory {

    VECTOR_MUT {
        @Override
        public ClockController createController() {
            return new FlatController(MutableVector.make());
        }
    },
    VECTOR_IMUT {
        @Override
        public ClockController createController() {
            return new FlatController(PersistentVector.make());
        }
    },
    VECTOR_STACK_MUT {
        @Override
        public ClockController createController() {
            return new StackedController(MutableVector.make());
        }
    },
    VECTOR_STACK_IMUT {
        @Override
        public ClockController createController() {
            return new StackedController(PersistentVector.make());
        }
    },
    PREFIX {
        @Override
        public ClockController createController() {
            return new PrefixController();
        }
    };

    public abstract ClockController createController();
}
