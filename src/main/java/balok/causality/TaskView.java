package balok.causality;

import balok.vc.AllToAllOrdering;
import balok.vc.PhaseOrdering;

import java.util.Optional;

/**
 * Represents a collection of events. Events can be merge together and compared
 * for reachability.
 */
public class TaskView<T> implements Event<Epoch<T>> {
    private final Event<T> local;
    private final PhaseOrdering global;
    private final int globalTime;
    private final AllToAllOrdering cyclic;

    public TaskView(Event<T> local, PhaseOrdering global, AllToAllOrdering cyclic) {
        this(0, local, global, cyclic);
    }

    public TaskView(int globalTime, Event<T> local, PhaseOrdering global, AllToAllOrdering cyclic) {
        this.globalTime = globalTime;
        this.local = local;
        this.global = global;
        this.cyclic = cyclic;
    }

    /**
     * Compresses the information in a task view.
     *
     * @return
     */
    public Epoch<T> project() {
        return new Epoch<>(globalTime, local.project(), global, cyclic);
    }

    @Override
    public String toString() {
        return "<L=" + local + ", G=" + global + ">";
    }

    public Event<T> getLocal() {
        return local;
    }

    public PhaseOrdering getGlobal() {
        return global;
    }

    public int getGlobalTime() {
        return globalTime;
    }

    public AllToAllOrdering getCyclic() {
        return cyclic;
    }

    @Override
    public Optional<Causality> observe(Epoch<T> epoch) {
        Optional<Causality> result = local.observe(epoch.getLocal());
        if (result.isPresent()) {
            return result;
        }
        return ((cyclic != null && epoch.getCyclic() != null && epoch.getCyclic().happensBefore(cyclic)) ||
                (global != null && epoch.getGlobal() != null && epoch.getGlobal().happensBefore(global)) ||
                epoch.getGlobalTime() < globalTime) ? Optional.of(Causality.HAPPENED_AFTER) : Optional.empty();
    }
}
