package balok.causality;

public final class TimestampCache {
    private static final int
        PTP = 1 << 0,
        PO = 1 << 1,
        ATA = 1 << 2;

    private byte state = 0;
    public void invalidatePointToPoint() {
        state |= PTP;
    }
    public void invalidatePhaseOrdering() {
        state |= PO;
    }
    public void invalidateAllToAll() {
        state |= ATA;
    }
    public void invalidate() {
        state = PTP | PO | ATA;
    }
    public boolean createPointToPoint() {
        return (state & PTP) != 0;
    }
    public boolean createAllToAll() {
        return (state & ATA) != 0;
    }
    public boolean createPhaseOrdering() {
        return (state & PO) != 0;
    }

    public boolean isInvalid() {
        return state != 0;
    }

    public void reset() {
        state = 0;
    }
}
