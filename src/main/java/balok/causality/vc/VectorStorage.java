package balok.causality.vc;

public interface VectorStorage extends VectorView {
    VectorBuilder edit();
    VectorStorage empty();
}
