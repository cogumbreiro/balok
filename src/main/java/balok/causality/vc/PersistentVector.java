package balok.causality.vc;

import balok.vc.PersistentVectorClock;

public class PersistentVector implements VectorStorage, VectorBuilder {
    private PersistentVectorClock clock;
    private static final PersistentVector EMPTY = new PersistentVector();

    public PersistentVector(PersistentVectorClock clock) {
        this.clock = clock;
    }

    private PersistentVector() {
        this.clock = PersistentVectorClock.empty();
    }

    public static VectorStorage make() {
        return EMPTY;
    }

    @Override
    public VectorStorage empty() {
        return EMPTY;
    }

    @Override
    public void add(VectorEpoch epoch) {
        clock = clock.mergeEntry(epoch.getId(), epoch.getTime());
    }

    @Override
    public void addAll(VectorStorage obj) {
        PersistentVector other = (PersistentVector) obj;
        clock = clock.merge(other.clock);
    }

    @Override
    public VectorStorage build() {
        return new PersistentVector(clock);
    }

    @Override
    public VectorBuilder edit() {
        return new PersistentVector(clock);
    }

    @Override
    public int getTime(int id) {
        return clock.getTime(id);
    }

    @Override
    public String toString() {
        return clock.toString();
    }
}
