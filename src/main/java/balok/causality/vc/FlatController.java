package balok.causality.vc;

import balok.causality.ClockController;
import balok.causality.Event;

/**
 * Performs joins tracking on joins operations.
 * 
 * @author Tiago Cogumbreiro
 *
 */
public class FlatController implements ClockController<VectorEpoch>, VectorEvent {
    /**
     * Defint
     */
    private final VectorEpoch epoch;
	private final VectorStorage joins;

    public FlatController(VectorStorage storage) {
        this(new VectorEpoch(), storage);
    }

	/**
	 * Creates a new task tracker given a phase ordering.
	 */
	protected FlatController(VectorEpoch epoch, VectorStorage storage) {
	    this.epoch = epoch;
		this.joins = storage;
	}

	@Override
	public FlatController spawnChild() {
        VectorBuilder newJoins = joins.edit();
        newJoins.add(epoch);
        return new FlatController(epoch.spawn(), newJoins.build());
	}

	@Override
	public FlatController join(Event<VectorEpoch> obj) {
        FlatController other = (FlatController) obj;
        if (observe(other.epoch).isPresent()) {
            // the other is already contained in this vector
            return this;
        }
        VectorBuilder newLocal = joins.edit();
        // only perform a merge if we are behind the last join, otherwise
        // we already joined with this task
        if (getTime(other.epoch.getId()) < other.epoch.getLastJoin()) {
            newLocal.addAll(other.joins);
        }
        // add the current entry
        newLocal.add(other.epoch);
        return new FlatController(epoch.join(), newLocal.build());
	}

    @Override
	public FlatController produceEvent() {
        return new FlatController(epoch.tick(), joins);
	}

    @Override
    public VectorEpoch project() {
        return epoch;
    }

    @Override
	public FlatController createView() {
		return this;
	}

	@Override
	public String toString() {
		return "{current:" + epoch + ", joins:" + joins + "}";
	}

    @Override
    public int getTime(int id) {
        return this.epoch.getId() == id ? this.epoch.getTime() : joins.getTime(id);
    }
}
