package balok.causality.vc;

import balok.causality.ClockController;
import balok.causality.Event;
import gorn.vc.MapUtil;

import java.util.ArrayList;

/**
 * Performs local tracking on local operations.
 * 
 * @author Tiago Cogumbreiro
 *
 */
public class StackedController implements ClockController<VectorEpoch>, VectorEvent {
    private final VectorEpoch epoch;
	private final VectorStorage joins;
    private final StackedController parent;

	/**
	 * Creates a new task tracker given a phase ordering.
	 */
	public StackedController(VectorStorage empty) {
	    this.epoch = new VectorEpoch();
		this.joins = empty;
		this.parent = null;
	}

    protected StackedController(VectorEpoch epoch, VectorStorage joins, StackedController parent) {
        this.epoch = epoch;
        this.joins = joins;
        this.parent = parent;
    }

    /**
	 * Creates a deep copy of the current handler.
	 * This must be invoked <strong>before</strong> the local is created and launched.
	 *
	 * @return
	 */
	public StackedController spawnChild() {
	    return new StackedController(epoch.spawn(), joins.empty(), this);
	}

	/**
	 * Should be executed after a local joins with another local.
	 */
	@Override
	public StackedController join(Event<VectorEpoch> obj) {
        StackedController other = (StackedController) obj;
        if (observe(other.epoch).isPresent()) {
            // we already merged with this task before, no need to do it again
            return this;
        }
        VectorBuilder newJoins = joins.edit();
        mergeInto(newJoins, other);
        return new StackedController(epoch.join(), newJoins.build(), parent);
	}

	private void mergeInto(VectorBuilder target, StackedController other) {
	    ArrayList<VectorStorage> storage = new ArrayList<>();
	    do {
	        if (observe(other.epoch).isPresent()) {
	            // nothing else to do as we already have the rest
	            return;
            }
            target.add(other.epoch);
	        // only perform a merge if we are behind the last join, otherwise
            // we already joined with this task
            if (other.joins != null && getTime(other.epoch.getId()) < other.epoch.getLastJoin()) {
                target.addAll(other.joins);
            }
            other = other.parent;
        } while (other != null);
    }

    /**
	 * Produces an observable event.
	 */
	public StackedController produceEvent() {
		return new StackedController(epoch.tick(), joins, parent);
	}

	@Override
	public boolean equals(Object vec) {
		if (this == vec) {
			return true;
		}
		if (!(vec instanceof StackedController)) {
			return false;
		}
		StackedController other = (StackedController) vec;
		return epoch.equalsTo(other.epoch);
	}

    @Override
    public VectorEpoch project() {
        return epoch;
    }

    /**
	 * Produces a timestamp of the current local.
	 * @return
	 */
	public StackedController createView() {
		return this;
	}

	@Override
	public String toString() {
		return "{\"current\": " + epoch +
                (joins != null ? ", \"joins\": " + joins : "") +
                (parent != null ? ", \"parent\": " + parent : "") + "}";
	}

	@Override
    public int getTime(int id) {
	    if (id == this.epoch.getId()) {
            return this.epoch.getTime();
        }
        int result = MapUtil.ZERO;
	    StackedController elem = this;
	    do {
            if (elem.epoch.getId() == id) {
                result = Math.max(elem.epoch.getTime(), result);
            }
            result = Math.max(result, elem.joins != null ? elem.joins.getTime(id) : result);
            elem = elem.parent;
        } while (elem != null);
        return result;
    }
}
