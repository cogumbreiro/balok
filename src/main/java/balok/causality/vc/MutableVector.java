package balok.causality.vc;

import gorn.vc.MapUtil;
import gorn.vc.VectorClock;

public class MutableVector implements VectorStorage, VectorBuilder {
    private VectorClock clock;
    private boolean building;

    private MutableVector() {
        building = false;
    }

    private static final MutableVector EMPTY = new MutableVector();

    @Override
    public VectorStorage empty() {
        return EMPTY;
    }

    public static VectorStorage make() {
        return EMPTY;
    }

    public MutableVector(VectorClock clock) {
        this.clock = clock;
        this.building = true;
    }

    @Override
    public void add(VectorEpoch epoch) {
        assert building;
        clock.mergeEntry(epoch.getId(), epoch.getTime());
    }

    @Override
    public void addAll(VectorStorage obj) {
        assert building;
        MutableVector other = (MutableVector) obj;
        if (other.clock != null) clock.merge(other.clock);
    }

    @Override
    public VectorStorage build() {
        assert building;
        building = false;
        return this;
    }

    @Override
    public VectorBuilder edit() {
        assert !building;
        return new MutableVector(clock == null ? new VectorClock() : clock.clone());
    }

    @Override
    public int getTime(int id) {
        return clock != null ? clock.getTime(id) : MapUtil.ZERO;
    }

    @Override
    public String toString() {
        return clock == null ? "{}" : clock.toString();
    }
}
