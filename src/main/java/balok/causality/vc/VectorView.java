package balok.causality.vc;

public interface VectorView {
    int getTime(int id);
}
