package balok.causality.vc;

import gorn.vc.MapUtil;

import java.util.concurrent.atomic.AtomicInteger;

public class VectorEpoch {
    private final AtomicInteger idGen;
    private final int id;
    private final int time;
    private final int lastJoin;

    private VectorEpoch(AtomicInteger idGen, int id, int time, int lastJoin) {
        this.id = id;
        this.time = time;
        this.idGen = idGen;
        this.lastJoin = lastJoin;
    }

    public VectorEpoch() {
        this.idGen = new AtomicInteger();
        this.id = idGen.getAndIncrement();
        this.time = MapUtil.ZERO + 1;
        this.lastJoin = this.time;
    }

    public VectorEpoch spawn() {
        int newId = idGen.getAndIncrement();
        int newTime = MapUtil.ZERO + 1;
        return new VectorEpoch(idGen, newId, newTime, newTime);
    }

    public VectorEpoch tick() {
        return new VectorEpoch(idGen, id, time + 1, lastJoin);
    }

    public VectorEpoch join() {
        int newTime = this.time + 1;
        return new VectorEpoch(idGen, id, newTime, newTime);
    }

    public int getId() { return id; }

    public int getTime() { return time; }

    public int getLastJoin() { return lastJoin; }

    public boolean happensBeforeRefl(VectorEpoch other) {
        return this.id == other.id && this.time <= other.time;
    }

    public boolean equalsTo(VectorEpoch other) {
        return id == other.id && time == other.time;
    }

    @Override
    public String toString() {
        return "{" + id + ":" + MapUtil.asLong(time) + ", 'lastJoin':" + MapUtil.asLong(lastJoin) + "}";
    }
}
