package balok.causality.vc;

import balok.causality.Causality;
import balok.causality.Event;

import java.util.Optional;

public interface VectorEvent extends VectorView, Event<VectorEpoch> {
    @Override
    default Optional<Causality> observe(VectorEpoch epoch) {
        int result = Integer.compare(getTime(epoch.getId()), epoch.getTime());
        if (result > 0) return Causality.SOME_HAPPENED_AFTER;
        if (result == 0) return Causality.SOME_EQ;
        return Causality.NONE;
    }
}
