package balok.causality.vc;

public interface VectorBuilder {
    void add(VectorEpoch epoch);
    void addAll(VectorStorage other);
    VectorStorage build();
}
