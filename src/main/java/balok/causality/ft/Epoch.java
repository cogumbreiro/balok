package balok.causality.ft;

import balok.vc.AllToAllOrdering;
import balok.vc.PhaseOrdering;

/**
 * A projection of a task view: aggregates a local event, plus phase-ordering causality, cyclic causality, and global
 * causality.
 */
public class Epoch<T> {
    private final T local;
    private final AllToAllOrdering cyclic;

    public Epoch(T local, AllToAllOrdering cyclic) {
        this.local = local;
        this.cyclic = cyclic;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof TaskView)) {
            return false;
        }
        TaskView other = (TaskView) obj;
        return local.equals(other.getLocal());
    }

    @Override
    public String toString() {
        return "<L=" + local + ", G=" + cyclic + ">";
    }

    public T getLocal() {
        return local;
    }

    public AllToAllOrdering getCyclic() {
        return cyclic;
    }
}
