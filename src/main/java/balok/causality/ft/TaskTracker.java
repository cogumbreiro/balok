package balok.causality.ft;

import balok.causality.ClockController;
import balok.causality.TimestampCache;
import balok.vc.AllToAllOrdering;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Performs causality tracking on task operations.
 *
 * @author Tiago Cogumbreiro
 */
public class TaskTracker<T> {
    private ClockController<T> ptp;
    private final AllToAllOrdering cyclic;
    private final TimestampCache cacheHandler = new TimestampCache();
    private TaskView<T> cache = null;

    /**
     * Creates a new task tracker given a phase ordering.
     *
     */
    public TaskTracker(ClockController<T> root) {
        this(root, new AllToAllOrdering());
    }

    private TaskTracker(ClockController local, AllToAllOrdering cyclicOrdering) {
        this.ptp = local;
        this.cyclic = cyclicOrdering;
    }

    /**
     * Must be invoked before a parent task spawns a child task.
     *
     * @return
     */
    public TaskTracker<T> createChild() {
        return new TaskTracker<>(ptp.spawnChild(), cyclic.clone());
    }

    /**
     * Must be executed at the parent site <strong>after</strong> spawning a task.
     */
    public void afterSpawn() {
        produceEvent();
    }

    /**
     * Merges a target timestamp with the current task; advances
     * the local time of this task.
     */
    public void join(TaskView<T> other) {
        ClockController<T> join = ptp.join(other.getLocal());
        if (ptp == join) {
            // returns the same instance if the task already joined
            return;
        }
        ptp = join;
        cacheHandler.invalidatePointToPoint();
        if (cyclic.merge(other.getCyclic())) {
            cacheHandler.invalidateAllToAll();
        }
    }

    public void beforeBarrier(int barrierId) {
        // Nothing to do
    }

    public void afterBarrier(int barrierId) {
        cyclic.awaitAdvance(barrierId);
        cacheHandler.invalidateAllToAll();
    }

    /**
     * Produces an observable event (advances the local time of the current task).
     */
    public void produceEvent() {
        ptp = ptp.produceEvent();
        cacheHandler.invalidatePointToPoint();
    }

    /**
     * Produces a timestamp of the current task.
     *
     * @return
     */
    public TaskView<T> createTimestamp() {
        if (cache == null) {
            cache = new TaskView<>(ptp.createView(), cyclic.clone());
        } else if (cacheHandler.isInvalid()) {
            cache = new TaskView<>(ptp.createView(),
                    cacheHandler.createAllToAll() ? cyclic.clone() : cache.getCyclic());
        }
        return cache;
    }

    @Override
    public String toString() {
        return "TaskTracker[L=" + ptp + ", B=" + cyclic + "]";
    }
}
