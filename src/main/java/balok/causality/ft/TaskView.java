package balok.causality.ft;

import balok.causality.Causality;
import balok.causality.Event;
import balok.vc.AllToAllOrdering;
import balok.vc.PhaseOrdering;

import java.util.Optional;

/**
 * Represents a collection of events. Events can be merge together and compared
 * for reachability.
 */
public class TaskView<T> implements Event<Epoch<T>> {
    private final Event<T> local;
    private final AllToAllOrdering cyclic;

    public TaskView(Event<T> local, AllToAllOrdering cyclic) {
        this.local = local;
        this.cyclic = cyclic;
    }

    /**
     * Compresses the information in a task view.
     *
     * @return
     */
    public Epoch<T> project() {
        return new Epoch<>(local.project(), cyclic);
    }

    @Override
    public String toString() {
        return "<L=" + local + ", G=" + cyclic + ">";
    }

    public Event<T> getLocal() {
        return local;
    }

    public AllToAllOrdering getCyclic() {
        return cyclic;
    }

    @Override
    public Optional<Causality> observe(Epoch<T> epoch) {
        Optional<Causality> result = local.observe(epoch.getLocal());
        if (result.isPresent()) {
            return result;
        }
        return (cyclic != null && epoch.getCyclic() != null && epoch.getCyclic().happensBefore(cyclic))
                ? Optional.of(Causality.HAPPENED_AFTER) : Optional.empty();
    }
}
