package balok.causality;

import balok.vc.AllToAllOrdering;
import balok.vc.PhaseOrdering;

/**
 * A projection of a task view: aggregates a local event, plus phase-ordering causality, cyclic causality, and global
 * causality.
 */
public class Epoch<T> {
    private final T local;
    private final PhaseOrdering global;
    private final int globalTime;
    private final AllToAllOrdering cyclic;

    public Epoch(int globalTime, T local, PhaseOrdering global, AllToAllOrdering cyclic) {
        this.globalTime = globalTime;
        this.local = local;
        this.global = global;
        this.cyclic = cyclic;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof TaskView)) {
            return false;
        }
        TaskView other = (TaskView) obj;
        return local.equals(other.getLocal());
    }

    @Override
    public String toString() {
        return "<L=" + local + ", G=" + global + ">";
    }

    public T getLocal() {
        return local;
    }

    public PhaseOrdering getGlobal() {
        return global;
    }

    public int getGlobalTime() {
        return globalTime;
    }

    public AllToAllOrdering getCyclic() {
        return cyclic;
    }
}
