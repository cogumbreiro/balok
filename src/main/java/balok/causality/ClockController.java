package balok.causality;

/**
 * Conceptually pairs a participant with a logical clock.
 */
public interface ClockController<T> {
    /**
     * Copies this logical clock and creates a participant; the new participant
     * produces one event.
     *
     * This must be invoked <strong>before</strong> the local is created and launched.
     *
     * The child must happen after the event at which the creation happens.
     * @return
     */
    ClockController<T> spawnChild();

    /**
     * Should be executed after a local joins with another local.
     * The result happesn before the creator.
     * @return Can return this in case there is no change needed.
     */
    ClockController<T> join(Event<T> other);

    /**
     * Produces an observable event.
     * Must be invoked after a task spawns another,
     * and after a task joins with another.
     */
    ClockController<T> produceEvent();

    /**
     * Must be invoked after a task spawns another,
     * @return <code>produceEvent()</code>
     */
    default ClockController<T> afterSpawn() {
        return produceEvent();
    }

    /**
     * Produces a snapshot of the current logical clock.
     * @return
     **/
    Event<T> createView();
}
