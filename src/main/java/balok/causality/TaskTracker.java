package balok.causality;

import balok.vc.CollectiveClock;
import balok.vc.AllToAllOrdering;
import balok.vc.PhaseOrdering;
import balok.vc.RegistrationMode;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Performs causality tracking on task operations.
 *
 * @author Tiago Cogumbreiro
 */
public class TaskTracker<T> {
    private final static AtomicInteger ID_GENERATOR = new AtomicInteger();
    private final static AtomicInteger GLOBAL_TIME = new AtomicInteger();
    private int globalTime;
    private ClockController<T> ptp;
    private CollectiveClock collective;
    private final AllToAllOrdering cyclic;
    private final TimestampCache cacheHandler = new TimestampCache();
    private TaskView<T> cache = null;

    /**
     * Creates a new task tracker given a phase ordering.
     *
     * @param globalTime
     */
    public TaskTracker(ClockController<T> root, int globalTime) {
        this(globalTime, root, null, new AllToAllOrdering());
    }

    /**
     * Creates a new task tracker observe with respect to the last root-task tracker.
     */
    public TaskTracker(ClockController<T> root) {
        this(root, GLOBAL_TIME.getAndIncrement());
    }

    private TaskTracker(int globalTime, ClockController local, PhaseOrdering global, AllToAllOrdering cyclicOrdering) {
        this.globalTime = globalTime;
        this.ptp = local;
        if (global != null) {
            this.collective = new CollectiveClock(global);
        }
        this.cyclic = cyclicOrdering;
    }

    /**
     * Must be invoked before a parent task spawns a child task.
     *
     * @return
     */
    public TaskTracker<T> createChild() {
        PhaseOrdering newCollective = collective != null ? collective.createTimestamp() : null;
        return new TaskTracker<>(globalTime, ptp.spawnChild(), newCollective, cyclic.clone());
    }

    /**
     * Must be executed at the parent site <strong>after</strong> spawning a task.
     */
    public void afterSpawn() {
        produceEvent();
    }

    /**
     * Merges a target timestamp with the current task; advances
     * the local time of this task.
     */
    public void join(TaskView<T> other) {
        ClockController<T> join = ptp.join(other.getLocal());
        if (ptp == join) {
            // returns the same instance if the task already joined
            return;
        }
        ptp = join;
        cacheHandler.invalidatePointToPoint();
        if (other.getGlobal() != null) {
            if (collective == null) {
                collective = new CollectiveClock(other.getGlobal().clone());
                cacheHandler.invalidatePhaseOrdering();
            } else if (collective.merge(other.getGlobal())) {
                cacheHandler.invalidatePhaseOrdering();
            }
        }
        if (cyclic.merge(other.getCyclic())) {
            cacheHandler.invalidateAllToAll();
        }
    }

    /**
     * Produces an observable event (advances the local time of the current task).
     */
    public void produceEvent() {
        ptp = ptp.produceEvent();
        cacheHandler.invalidatePointToPoint();
    }

    private PhaseOrdering getCollectiveTimestamp() {
        return collective != null ? collective.createTimestamp() : null;
    }

    /**
     * Produces a timestamp of the current task.
     *
     * @return
     */
    public TaskView<T> createTimestamp() {
        if (cache == null) {
            cache = new TaskView<>(globalTime, ptp.createView(), getCollectiveTimestamp(), cyclic.clone());
        } else if (cacheHandler.isInvalid()) {
            cache = new TaskView<>(globalTime,
                    ptp.createView(),
                    cacheHandler.createPhaseOrdering() ? getCollectiveTimestamp() : cache.getGlobal(),
                    cacheHandler.createAllToAll() ? cyclic.clone() : cache.getCyclic());
        }
        return cache;
    }

    private static int generateId() {
        return ID_GENERATOR.getAndIncrement();
    }

    /**
     * Should be invoked when the task creates a barrier.
     * Creating a barrier performs a signals event and a ptp event.
     *
     * @return
     */
    public PhaserTracker createPhaser() {
        if (collective == null) {
            collective = new CollectiveClock();
        }
        int barrierId = generateId();
        PhaserTracker barrierTracker = new PhaserTracker(barrierId, collective, cacheHandler);
        collective.signalAwait(barrierId);
        produceEvent(); // XXX: needed?
        cacheHandler.invalidatePhaseOrdering();
        return barrierTracker;
    }

    /**
     * Should be invoked when the task creates a barrier.
     * Creating a barrier performs a signals event and a ptp event.
     *
     * @return
     */
    public AllToAllTracker createBarrier() {
        int barrierId = generateId();
        AllToAllTracker barrierTracker = new AllToAllTracker(barrierId, cyclic);
        cyclic.awaitAdvance(barrierId);
        cacheHandler.invalidateAllToAll();
        produceEvent(); // XXX: needed?
        return barrierTracker;
    }

    /**
     * Registers this task in the target barrier.
     *
     * @param tracker
     * @return The barrier tracker for this task.
     */
    public PhaserTracker registerPhaser(PhaserTracker tracker, RegistrationMode mode) {
        if (collective == null) {
            collective = new CollectiveClock();
        }
        PhaserTracker barrier = new PhaserTracker(tracker.id, collective, cacheHandler);
        // copies the signal-wait phase from the target barrier
        barrier.globals.copyFrom(tracker.globals, tracker.id, mode);
        cacheHandler.invalidatePhaseOrdering();
        return barrier;
    }

    @Override
    public String toString() {
        return "TaskTracker[L=" + ptp + ", G=" + collective + ", B=" + cyclic + "]";
    }
}
