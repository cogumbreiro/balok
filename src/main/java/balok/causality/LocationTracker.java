package balok.causality;

/**
 * Tracks the reads and write events of a specific memory location.
 */
public class LocationTracker<A extends Access, T> {
    protected EpochSet<A, T> read = null;
    protected AccessEntry<A, T> write = null;

    protected final AccessEntry<A, T> getWriteConflict(AccessEntry<A, Event<T>> access) {
        Event<T> event = access.getValue();
        assert access.getAccess().getMode() == AccessMode.WRITE;
        if (read != null) {
            AccessEntry<A, T> conflict = read.lookupConflict(access.getValue());
            if (conflict != null) {
                return conflict;
            }
        }
        if (write != null && ! event.observe(write.getValue()).isPresent()) {
            return write;
        }
        return null;
    }

    /**
     * Tries to add a write to the location.
     * @param access The write event.
     * @return Returns non-null if there is a data-race.
     */
    private AccessEntry<A, T> tryAddWrite(AccessEntry<A, Event<T>> access) {
        AccessEntry<A, T> check = getWriteConflict(access);
        if (check != null) {
            return check;
        }
        addWrite(access);
        return null;
    }

    private void addWrite(AccessEntry<A, Event<T>> access) {
        write = access.setValue(access.getValue().project());
        // since the reads must be observe with the write, we can clear the reads
        read = null;
    }

    protected final AccessEntry<A, T> getReadConflict(AccessEntry<A, Event<T>> access) {
        Event<T> event = access.getValue();
        assert access.getAccess().getMode() == AccessMode.READ;
        if (read != null && read.contains(access.getValue())) {
            return null;
        }
        if (write != null && !event.observe(write.getValue()).isPresent()) {
            return write;
        }
        return null;
    }

    /**
     * Tries to add a read event to the location.
     * @param access
     * @return Returns false if there is a data race and does not add the event to this shadow variable.
     */
    private AccessEntry<A, T> tryAddRead(AccessEntry<A, Event<T>> access) {
        Event<T> event = access.getValue();
        assert access.getAccess().getMode() == AccessMode.READ;
        if (read != null && read.contains(access.getValue())) {
    		return null;
	    }
        if (write != null && ! event.observe(write.getValue()).isPresent()) {
            return write;
        }
        addRead(access);
        return null;
    }

    private void addRead(AccessEntry<A, Event<T>> access) {
        if (read == null) {
            read = new EpochSet<>();
        }
        read.add(access);
    }

    /**
     * Tries to add an event to the location.
     * @param access The memory access we are trying to add.
     * @return Returns false if there is a data race. In which case,
     * it does not add the event to this shadow variable.
     */
    public AccessEntry<A,T> lookupConflict(AccessEntry<A, Event<T>> access) {
        switch(access.getAccess().getMode()) {
            case READ:
                return getReadConflict(access);
            case WRITE:
                return getWriteConflict(access);
        }
        throw new IllegalStateException("Shouldn't reach here!");
    }

    /**
     * Adds an event to the location.
     * @param access The memory access we are trying to add.
     */
    public void unsafeAdd(AccessEntry<A, Event<T>> access) {
        switch(access.getAccess().getMode()) {
            case READ:
                addRead(access);
                return;
            case WRITE:
                addWrite(access);
                return;
        }
        throw new IllegalStateException("Shouldn't reach here!");
    }

    /**
     * Tests if any there was any element being added.
     * @return Returns <code>true</code> when no access have been added.
     */
    public boolean isEmpty() {
        return read == null && write == null;
    }

    /**
     * Returns the union of all reads.
     * @return The owner is this object; do not mutate.
     */
    public EpochSet<A,T> getReads() {
        return read;
    }

    /**
     * Returns the last write stored.s
     * @return The owner is this object; do not mutate.
     */
    public AccessEntry<A,T> getLastWrite() {
        return write;
    }

    @Override
    public String toString() {
        return "[read=" + read + ", write=" + write + "]";
    }
}
