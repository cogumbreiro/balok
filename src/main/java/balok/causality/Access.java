package balok.causality;

/**
 * An access holds an access-mode.
 */
public interface Access {
    AccessMode getMode();
}
