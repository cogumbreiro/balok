package balok.vc;

import gorn.vc.MapUtil;
import io.vavr.collection.HashMap;
import io.vavr.collection.Map;

/**
 * Straightforward implementation of a Vector Clock.
 * @author Tiago Cogumbreiro
 *
 */
public final class PersistentVectorClock {
    private static final Integer ZERO = MapUtil.ZERO;
    private final Map<Integer, Integer> vector;

	protected PersistentVectorClock(Map<Integer, Integer> view) {
		this.vector = view;
	}

	private static final PersistentVectorClock EMPTY = new PersistentVectorClock();

	public static PersistentVectorClock empty() {
	    return EMPTY;
    }

	private PersistentVectorClock() {
		this.vector = HashMap.empty();
    }
	
	public PersistentVectorClock advanceTime(int id) {
	    return new PersistentVectorClock(vector.put(id, getTime(id) + 1));
	}

	public int getTime(int id) {
        return vector.getOrElse(id, ZERO);
	}

	public PersistentVectorClock merge(PersistentVectorClock other) {
	    return new PersistentVectorClock(this.vector.merge(other.vector, Math::max));
	}

	public PersistentVectorClock mergeEntry(int id, int time) {
	    return new PersistentVectorClock(vector.put(id, Integer.max(time, getTime(id))));
    }

	@Override
	public String toString() {
		return vector.toString();
	}

	public boolean isEmpty() {
		return vector.isEmpty();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return false;
		}
		if (!(obj instanceof PersistentVectorClock)) {
			return false;
		}
		PersistentVectorClock other = (PersistentVectorClock) obj;
		return vector.equals(other.vector);
	}

    @Override
    public int hashCode() {
        return vector.hashCode();
    }
}
