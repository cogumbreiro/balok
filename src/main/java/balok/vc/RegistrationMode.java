package balok.vc;

public enum RegistrationMode {
    SIGNAL,
    WAIT,
    SIGNAL_WAIT
}
