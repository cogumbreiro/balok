package balok.vc;

import com.carrotsearch.hppc.IntIntHashMap;
import com.carrotsearch.hppc.cursors.IntIntCursor;
import balok.causality.Causality;
import balok.causality.Event;
import gorn.vc.MapUtil;

import java.util.Optional;

/**
 * Implements the phase-ordering happens-before relation.
 */
public class PhaseOrdering implements Event<PhaseOrdering> {
    private final IntIntHashMap signals;
    private final IntIntHashMap waits;

    /**
     * Creates a new global view: no signals nor waits produced.
     */
    public PhaseOrdering() {
        signals = new IntIntHashMap();
        waits = new IntIntHashMap();
    }

    protected PhaseOrdering(IntIntHashMap signals, IntIntHashMap waits) {
        this.signals = signals;
        this.waits = waits;
    }

    @Override
    public PhaseOrdering project() {
        return this;
    }

    @Override
    public Optional<Causality> observe(PhaseOrdering epoch) {
        return epoch.happensBefore(this) ? Optional.of(Causality.HAPPENED_AFTER) : Optional.empty();
    }

    /**
     * Increment the number of signals.
     * @param id
     */
    public void signal(int id) {
        MapUtil.increment(this.signals, id);
    }

    public void setSignalPhase(int id, int time) {
        signals.put(id, time);
    }

    public int getSignalPhase(int id) {
        return signals.get(id);
    }

    public void setWaitPhase(int id, int time) {
        waits.put(id, time);
    }

    public int getWaitPhase(int id) {
        return waits.get(id);
    }

    public void setSignalWaitPhase(int id, int time) {
        setWaitPhase(id, time);
        setSignalPhase(id, time);
    }

    /**
     * Observe the next global event.
     * @param id
     */
    public void await(int id) {
        MapUtil.increment(this.waits, id);
    }

    /**
     * Performs a signal and a wait.
     * @param id
     */
    public void signalAwait(int id) {
        MapUtil.increment(this.signals, id);
        MapUtil.increment(this.waits, id);
    }

    /**
     * Returns true if the two vector clocks are different and there is not a single possible ordering
     * among each other.
     * @param other
     * @return
     */
    public boolean isConcurrentWith(PhaseOrdering other) {
        return ! this.happensBefore(other) && !other.happensBefore(this);
    }

    /**
     * There is at least one element in the lhs that is greather than any element of the left-hand side.
     * @param other
     * @return
     */
    public boolean happensBefore(PhaseOrdering other) {
        for (IntIntCursor cursor : this.signals) {
            if (cursor.value < other.waits.getOrDefault(cursor.key, MapUtil.ZERO)) {
                return true;
            }
        }
        return false;
    }

    /**
     * There is at least one element in the lhs that is greather than any element of the left-hand side.
     * @param other
     * @return
     */
    public boolean reflexiveHappensBefore(PhaseOrdering other) {
        for (IntIntCursor cursor : this.signals) {
            int right = other.waits.getOrDefault(cursor.key, MapUtil.ZERO);
            if (cursor.value > right) {
                return false;
            }
            if (cursor.value < right) {
                return true;
            }
        }
        return true;
    }

    /**
     * Merge both views
     * @param other
     */
    public boolean merge(PhaseOrdering other) {
        boolean result = MapUtil.merge(this.signals, other.signals);
        result |= MapUtil.merge(this.waits, other.waits);
        return result;
    }

    @Override
    public PhaseOrdering clone() {
        return new PhaseOrdering(signals.clone(), waits.clone());
    }

    @Override
    public String toString() {
        return "PO[sp=" + MapUtil.toString(signals) + ", wp=" + MapUtil.toString(waits) + "]";
    }

    public boolean isEmpty() {
        return signals.isEmpty() && waits.isEmpty();
    }
}
