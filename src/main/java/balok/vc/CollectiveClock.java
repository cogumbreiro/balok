package balok.vc;

/**
 * A global clock keeps track of two phase-ordering objects:
 * a local phase-ordering timestamp that the task is operating,
 * and a global phase-ordering timestamp where merges are performed.
 * A global clock performs signals and awaits on the local phase-ordering
 * timestamp and performs merges on the global phase-ordering timestamp.
 */
public class CollectiveClock {
    /**
     * Keep track of phase-ordering
     */
    private final PhaseOrdering local = new PhaseOrdering();
    private final PhaseOrdering global;

    public CollectiveClock() {
        this(new PhaseOrdering());
    }

    /**
     * Creates a global clock from a phase-ordering timestamp.
     * The local timestamp is always initialized as empty.
     * @param timestamp This class will now own <code>timestamp</code>.
     */
    public CollectiveClock(PhaseOrdering timestamp) {
        this.global = timestamp;
    }

    /**
     * Merges a phase-ordering timestamp with the global timestamp.
     * @param other
     */
    public boolean merge(PhaseOrdering other) {
        return global.merge(other);
    }

    /**
     * Creates a timestamp object, which is just a merge of the local and the global timestamps.
     * @return
     */
    public PhaseOrdering createTimestamp() {
        if (global.isEmpty()) {
            return local.clone();
        }
        if (local.isEmpty()) {
            return global.clone();
        }
        PhaseOrdering timestamp = global.clone();
        timestamp.merge(local);
        return timestamp;
    }

    /**
     * Performs a signal and a wait.
     * @param id
     */
    public void signalAwait(int id) {
        local.signalAwait(id);
    }

    /**
     * Performs a signal.
     * @param id
     */
    public void signal(int id) {
        local.signal(id);
    }

    /**
     * Performs an await.
     * @param id
     */
    public void await(int id) {
        local.await(id);
    }

    /**
     * Copies the signal/wait phase from the given clock.
     * @param other
     * @param id
     * @param mode
     */
    public void copyFrom(CollectiveClock other, int id, RegistrationMode mode) {
        switch(mode) {
        case SIGNAL:
            local.setSignalPhase(id, other.local.getSignalPhase(id));
            break;
        case WAIT:
            local.setWaitPhase(id, other.local.getWaitPhase(id));
            break;
        case SIGNAL_WAIT:
            local.setSignalPhase(id, other.local.getSignalPhase(id));
            local.setWaitPhase(id, other.local.getWaitPhase(id));
            break;
        }
    }

    @Override
    public String toString() {
        return "GC[L=" + local + ", G=" + global + "]";
    }
}
