package balok.vc;

import com.carrotsearch.hppc.IntIntHashMap;
import com.carrotsearch.hppc.cursors.IntIntCursor;
import balok.causality.Causality;
import balok.causality.Event;
import gorn.vc.MapUtil;

import java.util.Optional;

/**
 * Implements the all-to-all happens-before relation.
 */
public class AllToAllOrdering implements Event<AllToAllOrdering> {
    private final IntIntHashMap phases;

    /**
     * Creates a new global view: no signals nor waits produced.
     */
    public AllToAllOrdering() {
        phases = new IntIntHashMap();
    }

    protected AllToAllOrdering(IntIntHashMap phases) {
        this.phases = phases;
    }

    /**
     * Increment the number of signals.
     * @param id
     */
    public void awaitAdvance(int id) {
        MapUtil.increment(this.phases, id);
    }

    public void setPhase(int id, int time) {
        phases.put(id, time);
    }

    public int getPhase(int id) {
        return phases.get(id);
    }

    @Override
    public AllToAllOrdering project() {
        return this;
    }

    @Override
    public Optional<Causality> observe(AllToAllOrdering o) {
        return o.happensBefore(this) ? Optional.of(Causality.HAPPENED_AFTER) : Optional.empty();
    }

    /**
     * Returns true if the two vector clocks are different and there is not a single possible ordering
     * among each other.
     * @param other
     * @return
     */
    public boolean isConcurrentWith(AllToAllOrdering other) {
        return ! this.happensBefore(other) && !other.happensBefore(this);
    }

    /**
     * There is at least one element in the lhs that is greather than any element of the left-hand side.
     * @param other
     * @return
     */
    public boolean happensBefore(AllToAllOrdering other) {
        for (IntIntCursor cursor : this.phases) {
            if (cursor.value < other.phases.getOrDefault(cursor.key, MapUtil.ZERO)) {
                return true;
            }
        }
        return false;
    }

    /**
     * There is at least one element in the lhs that is greather than any element of the left-hand side.
     * @param other
     * @return
     */
    public boolean reflexiveHappensBefore(AllToAllOrdering other) {
        for (IntIntCursor cursor : this.phases) {
            int right = other.phases.getOrDefault(cursor.key, MapUtil.ZERO);
            if (cursor.value > right) {
                return false;
            }
            if (cursor.value < right) {
                return true;
            }
        }
        return true;
    }

    /**
     * Merge both views
     * @param other
     */
    public boolean merge(AllToAllOrdering other) {
        return MapUtil.merge(this.phases, other.phases);
    }

    @Override
    public AllToAllOrdering clone() {
        return new AllToAllOrdering(phases.clone());
    }

    @Override
    public String toString() {
        return "Barriers" + MapUtil.toString(phases);
    }
}
