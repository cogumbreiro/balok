package gorn.vc;

import balok.vc.PhaseOrdering;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class PhaseOrderingTest {

	@Test
	public void example1() {
		// PO{1=>1, 2=>1} HB PO{1=>2, 2=>1}
		PhaseOrdering vc1 = new PhaseOrdering();
		PhaseOrdering vc2 = new PhaseOrdering();
		vc1.setSignalPhase(1, 1);
		vc1.setSignalPhase(2, 1);
		
		vc2.setWaitPhase(1, 2);
		vc2.setWaitPhase(2, 1);
		assertTrue(vc1.happensBefore(vc2));
	}

	@Test
	public void example2() {
		// PO{0=>1, 2=>1, 1=>2} HB PO{0=>3, 1=>3}
		PhaseOrdering left = new PhaseOrdering();
		left.setSignalPhase(0, 1);
		left.setSignalPhase(2, 1);
		left.setSignalPhase(1, 2);
		PhaseOrdering right = new PhaseOrdering();
		right.setWaitPhase(0, 3);
		right.setWaitPhase(1, 3);
		assertTrue(left.happensBefore(right));
	}

	@Test
	public void example3() {
		// PO{2=>2, 6=>2} HB PO{2=>5, 6=>2, 4=>2}
		PhaseOrdering l1 = new PhaseOrdering();
		l1.setSignalPhase(2, 2);
		l1.setSignalPhase(6, 2);
		PhaseOrdering l2 = new PhaseOrdering();
		l2.setWaitPhase(2, 5);
		l2.setWaitPhase(6, 2);
		l2.setWaitPhase(4, 2);
		assertTrue(l1.happensBefore(l2));
	}

	@Test
	public void example4() {
		// PO[sp={13=>1, 11=>1}, wp={13=>1, 11=>1}]
		// PAR
		// PO[sp={15=>1, 11=>1}, wp={15=>1, 11=>1}]
		PhaseOrdering g1 = new PhaseOrdering();
		g1.setSignalPhase(13, 1);
		g1.setSignalPhase(11, 1);
		g1.setWaitPhase(13, 1);
		g1.setWaitPhase(11, 1);

		PhaseOrdering g2 = new PhaseOrdering();
		g2.setSignalPhase(15, 1);
		g2.setSignalPhase(11, 1);
		g2.setWaitPhase(15, 1);
		g2.setWaitPhase(11, 1);

		assertTrue(g1.isConcurrentWith(g2));
	}
}
