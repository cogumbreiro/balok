package balok.causality.prefix;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class PrefixControllerTest {
    @Test
    public void bug1() {
        PrefixController ctrl =
                fromArray(4,5).join(
                    fromArray(1,7).join(
                        fromArray(3,3).join(
                            fromArray(2,3).join(
                                fromArray(4,3).join(fromArray(1,3))
                            )
                        )
                    )
                )
                ;
        assertTrue(fromArray(1,5) + " HB " + ctrl,
                PrefixController.happensBefore(SpawnPath.fromArray(1,5), ctrl));
    }

    private static PrefixController fromArray(int ...tid) {
        return new PrefixController(SpawnPath.fromArray(tid));
    }
}
