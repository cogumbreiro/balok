package balok.causality.prefix;

import org.junit.Test;

import java.util.Arrays;

import static junit.framework.TestCase.*;

public class TrieTest {
    @Test
    public void empty() {
        assertEquals(Arrays.asList(new SpawnPath(new int[0], 0)), new Trie().asList());
    }
    @Test
    public void example1() {
        Trie trie = new Trie();
        trie = trie.add(SpawnPath.fromArray(1));
        assertEquals(Arrays.asList(SpawnPath.fromArray(1)), trie.asList());
    }

    @Test
    public void example2() {
        Trie trie = new Trie();
        trie = trie.add(SpawnPath.fromArray(1));
        trie = trie.add(SpawnPath.fromArray(3));
        trie = trie.add(SpawnPath.fromArray(2));
        assertEquals(Arrays.asList(SpawnPath.fromArray(3)), trie.asList());
    }

    @Test
    public void example3() {
        Trie trie = new Trie();
        trie = trie.add(SpawnPath.fromArray(1,1));
        assertEquals(Arrays.asList(SpawnPath.fromArray(1,1)), trie.asList());
    }

    @Test
    public void example4() {
        Trie trie = new Trie();
        trie = trie.add(SpawnPath.fromArray(1,1));
        trie = trie.add(SpawnPath.fromArray(4,2));
        trie = trie.add(SpawnPath.fromArray(2));
        assertEquals(Arrays.asList(SpawnPath.fromArray(1,1), SpawnPath.fromArray(4,2)), trie.asList());
    }

    @Test
    public void example5() {
        Trie trie = new Trie();
        trie = trie.add(SpawnPath.fromArray(1,1));
        trie = trie.add(SpawnPath.fromArray(5,0));
        trie = trie.add(SpawnPath.fromArray(2));
        assertEquals(Arrays.asList(SpawnPath.fromArray(1,1), SpawnPath.fromArray(5,0)), trie.asList());
    }

    @Test
    public void merge1() {
        Trie left = new Trie();
        left = left.add(SpawnPath.fromArray(1,1));
        left = left.add(SpawnPath.fromArray(4,2));
        left = left.add(SpawnPath.fromArray(2));

        Trie right = new Trie();
        right = right.add(SpawnPath.fromArray(1,1));
        right = right.add(SpawnPath.fromArray(5,0));
        right = right.add(SpawnPath.fromArray(2));
        Trie trie = left.merge(right);
        assertEquals(Arrays.asList(SpawnPath.fromArray(1,1), SpawnPath.fromArray(4,2), SpawnPath.fromArray(5,0)), trie.asList());
    }

    // <L={"id":[0, 3, 1],"j":[[0, 1, 1], [0, 2, 2]]}, G=null>
    // Prev Timestamp: <L=[0, 2, 1], G=null>
    @Test
    public void bug1() {
        Trie left = new Trie();
        left = left.add(SpawnPath.fromArray(0,1,1));
        left = left.add(SpawnPath.fromArray(0,2,2));
        assertEquals(Arrays.asList(SpawnPath.fromArray(0,1,1), SpawnPath.fromArray(0,2,2)), left.asList());
        assertTrue(left.contains(SpawnPath.fromArray(0,2,1)));
    }
}
