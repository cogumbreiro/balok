package balok.causality.prefix;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static balok.causality.prefix.SpawnPath.fromArray;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SpawnPathTest {

    @Test
    public void hbOk() {
        for (SpawnPath tid : Arrays.asList(
                fromArray(1,2),
                fromArray(1,3,1),
                fromArray(1,3,2,4))) {
            assertHB(tid, fromArray(1,3,2,5));
        }
    }

    @Test
    public void happensAfter() {
	    assertEquals(Order.HAPPENS_AFTER, fromArray(1,3,2,5).match(fromArray(1,2)));
	    assertEquals(Order.HAPPENS_BEFORE, fromArray(1,2).match(fromArray(1,3,2,5)));
    }

    @Test
    public void hb() {
    	assertHB(fromArray(1,2),fromArray(1,3,2,5));
	    assertHB(fromArray(1,3,2,5), fromArray(1,3,2,6));
	    assertHB(fromArray(1), fromArray(2));
	    assertHB(fromArray(1,3,2), fromArray(1,3,2,1));
	    assertHB(fromArray(1), fromArray(2,3,1));
	    assertHB(fromArray(1), fromArray(1,0));
	    assertHB(fromArray(1), fromArray(10,9,8,7,6,5,4,3,2,1));
    }

    @Test
    public void par() {
	    assertMatches(Order.LT, fromArray(1,2,3), fromArray(1,3));
	    assertMatches(Order.GT, fromArray(2), fromArray(1,3,10,4));
	    assertMatches(Order.LT, fromArray(1,2,3), fromArray(1,3));
	    assertMatches(Order.GT, fromArray(3,2,1), fromArray(1,3,1));
	    assertMatches(Order.LT, fromArray(1,2,1), fromArray(1,3,1));
	    assertMatches(Order.LT, fromArray(1,1), fromArray(10,9,8,7,6,5,4,3,2,1));
    }

    @Test
    public void child() {
        assertTrue(fromArray(1,2,3).isParentOf(fromArray(1,2,0,0)));
        assertFalse(fromArray(1,2,0,0).isParentOf(fromArray(1,2,3)));
        assertFalse(fromArray(1,2,3).isParentOf(fromArray(1,1,0,0)));
    }

    static private void assertHB(SpawnPath left, SpawnPath right) {
        assertEquals(left + " < " + right, Order.HAPPENS_BEFORE, left.match(right));
	    assertEquals(right + " > " + left, Order.HAPPENS_AFTER, right.match(left));
        assertFalse("NOT " + right + " < " + left, right.match(left).happensBefore());
    }

    static private void assertMatches(Order expected, SpawnPath left, SpawnPath right) {
	    Order obtained = left.match(right);
	    assertEquals(left + " " + expected + " " + right, expected, obtained);
        assertEquals(right + " " + expected.symmetric() + " " + left, expected.symmetric(), right.match(left));
    }

    @Test
    public void list1() {
        ArrayList<SpawnPath> paths = new ArrayList<>();
        paths.add(fromArray(1));
        paths.add(fromArray(2));
        paths.add(fromArray(3));
        assertEquals(Arrays.asList(fromArray(3)), SpawnPath.asList(paths));
    }

    @Test
    public void list2() {
        ArrayList<SpawnPath> paths = new ArrayList<>();
        paths.add(fromArray(3));
        paths.add(fromArray(2));
        paths.add(fromArray(1));
        assertEquals(Arrays.asList(fromArray(3)), SpawnPath.asList(paths));
    }

    @Test
    public void list3() {
        ArrayList<SpawnPath> paths = new ArrayList<>();
        paths.add(fromArray(0));
        paths.add(fromArray(1,2,0));
        paths.add(fromArray(1,2,3));
        paths.add(fromArray(1,1));
        paths.add(fromArray(1,2,2));
        paths.add(fromArray(1,2,3));
        assertEquals(Arrays.asList(fromArray(1,2,3)), SpawnPath.asList(paths));
    }

    @Test
    public void list4() {
        ArrayList<SpawnPath> paths = new ArrayList<>();
        paths.add(fromArray(3,1));
        paths.add(fromArray(0));
        paths.add(fromArray(1,2,0));
        paths.add(fromArray(1,2,3));
        paths.add(fromArray(1,1));
        paths.add(fromArray(1,2,2));
        paths.add(fromArray(1,2,3));
        assertEquals(Arrays.asList(fromArray(1,2,3),fromArray(3,1)), SpawnPath.asList(paths));
    }

    @Test
    public void list5() {
        ArrayList<SpawnPath> paths = new ArrayList<>();
        paths.add(fromArray(3,3,3));
        paths.add(fromArray(1,2,1,3));
        paths.add(fromArray(1,2,3,4));
        paths.add(fromArray(1,2,2,0));
        assertEquals(Arrays.asList(
                fromArray(1,2,1,3),
                fromArray(1,2,2,0),
                fromArray(1,2,3,4),
                fromArray(3,3,3)), SpawnPath.asList(paths));
    }
}
