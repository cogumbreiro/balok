package balok.causality.prefix;

public class HashPrefixClockTest extends AbstractPrefixClockTest {
    @Override
    protected PrefixClock createClock() {
        return new HashPrefixClock();
    }
}
