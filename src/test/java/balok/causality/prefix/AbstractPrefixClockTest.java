package balok.causality.prefix;

import org.junit.Test;
import static balok.causality.prefix.SpawnPath.fromArray;
import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertSame;
import static org.junit.Assert.*;

public abstract class AbstractPrefixClockTest {
    abstract protected PrefixClock createClock();

	@Test
	public void example1() {
        PrefixClock expected = createClock();
        expected.add(fromArray(1,2,3));
        expected.add(fromArray(2,1,3));

        PrefixClock obtained = createClock();
		obtained.add(fromArray(1,2,3));
		obtained.add(fromArray(2,1,3));
		obtained.add(fromArray(2,1,2));
		obtained.add(fromArray(2,1,0));

		assertEquals(expected, obtained);
	}

	@Test
	public void example2() {
		PrefixClock map1 = createClock();
		map1.add(fromArray(1,2,3));
		map1.add(fromArray(2,1,3));

		PrefixClock map2 = createClock();
		map2.add(fromArray(1,2,4));
		map2.add(fromArray(2,1,2));

		PrefixClock expected = createClock();
		expected.add(fromArray(1,2,4));
        expected.add(fromArray(2,1,3));

		assertEquals(expected, map1.immutableMerge(map2));
	}

	@Test
	public void example3() {
		PrefixClock map1 = createClock();
		map1.add(fromArray(1,2,3));

		PrefixClock map2 = createClock();
		map2.add(fromArray(1,2,4));

		PrefixClock expected = createClock();
		expected.add(fromArray(1,2,4));

		assertEquals(expected, map1.immutableMerge(map2));
	}

    @Test
    public void example4() {
        PrefixClock expected = createClock();
        expected.add(fromArray(1,2,4));
        expected.add(fromArray(2,1,3));
        PrefixClock obtained = createClock();
        obtained.add(fromArray(2,1,3));
        obtained.add(fromArray(1,2,4));
        assertEquals(SpawnPath.asList(expected), SpawnPath.asList(obtained));
    }

    @Test
    public void emptyEquals() {
        assertEquals(createClock(), createClock());
    }

    @Test
    public void example6() {
        PrefixClock tree = createClock();
        SpawnPath tid1 = fromArray(1,2,3);
        tree.add(tid1);
        SpawnPath tid2 = fromArray(1,3,0);
        tree.add(tid2);
        assertSame(null, tree.find(fromArray(1,2,4)));
        assertSame(tid1, tree.find(fromArray(1,2,1)));
        assertSame(tid2, tree.find(fromArray(1,3)));
    }

    @Test
    public void example7() {
        PrefixClock tree = createClock();
        SpawnPath tid1 = fromArray(2,2,3);
        tree.add(tid1);
        SpawnPath tid2 = fromArray(1,3);
        tree.add(tid2);
        SpawnPath tid3 = fromArray(3,1);
        tree.add(tid3);
        SpawnPath tid4 = fromArray(2,1,2);
        tree.add(tid4);
        assertSame(tid4, tree.find(fromArray(2,1,0)));
    }

    @Test
    public void example8() {
        PrefixClock set1 = createClock();
        set1.add(fromArray(1,2,3));

        PrefixClock expected = createClock();
        expected.add(fromArray(1,2,4));

        assertEquals(expected, set1.immutableAdd(fromArray(1,2,4)));
    }

    @Test
    public void noUpdate() {
        TreePrefixClock tree = new TreePrefixClock();
        SpawnPath tid = fromArray(1,2,3);
        tree.add(tid);
        tree.add(fromArray(1,2));
        assertSame(tid, tree.find(tid));
    }


    @Test
    public void example10() {
        PrefixClock tree = createClock();
        SpawnPath tid1 = fromArray(2,2,3);
        tree.add(tid1);
        SpawnPath tid2 = fromArray(1,3);
        tree.add(tid2);
        SpawnPath tid3 = fromArray(3,1);
        tree.add(tid3);
        SpawnPath tid4 = fromArray(2,1,2);
        tree.add(tid4);
        assertSame(tid4, tree.find(fromArray(2,1,0)));
    }

    @Test
    public void example11() {
        PrefixClock set1 = createClock();
        set1.add(fromArray(1,2,3));

        PrefixClock expected = createClock();
        expected.add(fromArray(1,2,4));

        assertEquals(expected, set1.immutableAdd(fromArray(1,2,4)));
    }

}
