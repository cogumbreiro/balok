package balok.causality.prefix;

import org.junit.Test;

import static balok.causality.prefix.SpawnPath.fromArray;
import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertSame;

public class TreePrefixClockTest extends AbstractPrefixClockTest {
    @Override
    protected PrefixClock createClock() {
        return new TreePrefixClock();
    }

    // This is specific to a tree-spawn-path
    @Test
    public void checkFind() {
        PrefixClock tree = createClock();
        assertNull(tree.find(fromArray(1,2,3)));
        SpawnPath tid = fromArray(1,2,3);
        tree.add(tid);
        assertSame(tid, tree.find(tid));
    }

    @Test
    public void noUpdate() {
        TreePrefixClock tree = new TreePrefixClock();
        SpawnPath tid = fromArray(1,2,3);
        tree.add(tid);
        tree.add(fromArray(1,2));
        assertSame(tid, tree.find(tid));
    }


}
