package balok.causality;

import static org.junit.Assert.fail;

public class Util {

    public static void assertHB(TaskView t1, TaskView t2) {
        assertHB("", t1, t2);
    }

    private static boolean happensBefore(TaskView t1, TaskView t2) {
        return happensBefore(t1.project(), t2);
    }

    private static boolean happensBefore(Epoch c1, TaskView c2) {
        return c2.observe(c1).isPresent();
    }

    public static void assertHB(String message, TaskView t1, TaskView t2) {
        if (!happensBefore(t1, t2)) {
            if (happensBefore(t2, t1)) {
                fail(message + ": Expected " + t1.project() + " HB " + t2 + ", but got " + t2.project() + " HB " + t1);
            } else {
                fail(message + ": Expected " + t1.project() + " HB " + t2 + ", but got " + t1 + " PAR " + t2);
            }
        }
    }

    public static void assertPAR(TaskView t1, TaskView t2) {
        assertPAR("", t1, t2);
    }

    public static void assertPAR(String message, TaskView t1, TaskView t2) {
        if (happensBefore(t1, t2)) {
            if (happensBefore(t1, t2)) {
                fail(message + " because local is observe: " + t1.getLocal() + " < " + t2.getLocal());
            }
            if (t1.getGlobal().happensBefore(t2.getGlobal())) {
                fail(message + " because global is observe: " + t1.getGlobal() + " < " + t2.getGlobal());
            }
            if (t1.getGlobalTime() < t2.getGlobalTime()) {
                fail(message + " because global ticket is observe: " + t1.getGlobalTime() + " < " + t2.getGlobalTime());
            }
            throw new IllegalStateException("Should not reach here!");
        }

        if(happensBefore(t2, t1)) {
            if (happensBefore(t2, t1)) {
                fail(message + " because local is observe: " + t2.getLocal() + " < " + t1.getLocal());
            }
            if (t2.getGlobal().happensBefore(t1.getGlobal())) {
                fail(message + " because global is observe: " + t2.getGlobal() + " < " + t1.getGlobal());
            }
            if (t2.getGlobalTime() < t1.getGlobalTime()) {
                fail(message + " because global ticket is observe: " + t2.getGlobalTime() + " < " + t1.getGlobalTime());
            }
            throw new IllegalStateException("Should not reach here!");
        }
    }
}
