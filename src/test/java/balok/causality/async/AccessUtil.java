package balok.causality.async;

import balok.causality.Event;
import balok.causality.AccessEntry;

import static balok.causality.async.IAccess.READ;
import static balok.causality.async.IAccess.WRITE;

public class AccessUtil {
    public static AccessEntry<IAccess, Event<Integer>> readAccess(int value) {
        return new AccessEntry<>(READ, new IView(value));
    }

    public static AccessEntry<IAccess, Event<Integer>> writeAccess(int view) {
        return new AccessEntry<>(WRITE, new IView(view));
    }

    public static AccessEntry<IAccess, Integer> readEpoch(int value) {
        return new AccessEntry<>(READ, value);
    }

    public static AccessEntry<IAccess, Integer> writeEpoch(int view) {
        return new AccessEntry<>(WRITE, view);
    }

    public static Segment<IAccess, Integer> read(int view) {
        return Segment.make(readAccess(view));
    }

    public static IntervalTree<Segment<IAccess, Integer>> writeTree(int view, int start, int end) {
        return Segment.makeInterval(Segment.make(writeAccess(view)), new Interval(start, end));
    }

    public static IntervalTree<Segment<IAccess, Integer>> readTree(int view, int start, int end) {
        return Segment.makeInterval(Segment.make(readAccess(view)), new Interval(start, end));
    }

    public static Segment<IAccess, Integer> write(int view) {
        return Segment.make(writeAccess(view));
    }
}
