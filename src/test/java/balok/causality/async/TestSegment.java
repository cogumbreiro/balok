package balok.causality.async;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TestSegment {

    Segment<IAccess, Integer> acc1;
    Segment<IAccess, Integer> acc2;
    Segment<IAccess, Integer> acc3;

    @Before
    public void setUp() {
        acc1 = null;
        acc2 = null;
        acc3 = null;
    }

    @Test
    public void happyPathWW() {
        acc1 = AccessUtil.write(3);
        acc2 = AccessUtil.write(4);
        acc1.accept(acc2);
        assertEquals(Arrays.asList(), acc1.getReads());
        assertEquals(Arrays.asList(AccessUtil.writeEpoch(4)), acc1.getWrites());
    }

    @Test
    public void raceWW() {
        try {
            acc1 = AccessUtil.write(3);
            acc2 = AccessUtil.write(4);
            acc2.accept(acc1);
            fail();
        } catch (IllegalStateException e) {
            // OK
        }
    }

    @Test
    public void example1() {
        acc1 = AccessUtil.write(3);
        acc2 = AccessUtil.read(2);
        acc2.accept(acc1);
        assertEquals(Arrays.asList(), acc2.getReads());
        assertEquals(Arrays.asList(AccessUtil.writeEpoch(3)), acc2.getWrites());
        assertEquals(Arrays.asList(AccessUtil.readAccess(2)), acc2.getPendingReads());
        assertEquals(Arrays.asList(AccessUtil.writeAccess(3)), acc2.getPendingWrites());

        acc3 = AccessUtil.write(4);
        try {
            acc3.accept(acc2);
            fail();
        } catch (IllegalStateException e) {
            // OK
        }
    }

    @Test
    public void happyPathRW() {
        acc1 = AccessUtil.write(3);
        acc2 = AccessUtil.read(4);
        acc1.accept(acc2);
        assertEquals(Arrays.asList(AccessUtil.readEpoch(4)), acc1.getReads());
        assertEquals(Arrays.asList(AccessUtil.writeEpoch(3)), acc1.getWrites());
    }

    @Test
    public void raceRW() {
        acc1 = AccessUtil.write(3);
        acc2 = AccessUtil.read(4);
        try {
            acc2.accept(acc1);
            fail();
        } catch (IllegalStateException e) {
            // OK
        }
    }
}
