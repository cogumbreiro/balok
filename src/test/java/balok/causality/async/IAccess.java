package balok.causality.async;

import balok.causality.Access;
import balok.causality.AccessMode;

public class IAccess implements Access {
    private final AccessMode mode;

    private IAccess(AccessMode mode) {
        this.mode = mode;
    }

    @Override
    public AccessMode getMode() {
        return mode;
    }

    @Override
    public String toString() {
        return mode.toString();
    }

    public static final IAccess READ = new IAccess(AccessMode.READ);
    public static final IAccess WRITE = new IAccess(AccessMode.WRITE);
}
