package balok.causality.async;

import balok.causality.AccessEntry;
import org.junit.Test;

import static org.junit.Assert.*;

public class ShadowMemoryTest {
    @Test
    public void addDataRace1() {
        FrameBuilder<IAccess, Integer> task1 = new FrameBuilder<>(100);
        IView event = new IView(100);
        AsyncLocationTracker<IAccess, Integer> loc = new AsyncLocationTracker<>();
        task1.add(loc, new AccessEntry<>(IAccess.WRITE, event), AsyncLocationTracker.TICKET_ZERO);
        Frame<IAccess, Integer> frame1 = task1.build();

        // This access happened after and does not observe first write
        FrameBuilder<IAccess, Integer> task2 = new FrameBuilder<>(100);
        task2.add(loc, new AccessEntry<>(IAccess.WRITE, new IView(40)), AsyncLocationTracker.TICKET_ZERO + 1);
        Frame<IAccess, Integer> frame2 = task2.build();

        ShadowMemory<IAccess, Integer> mem = new ShadowMemory<>();
        frame2.addTo(mem);
        try {
            frame1.addTo(mem);
            fail();
        } catch (IllegalStateException e) {
            // OK
        }
    }

    @Test
    public void addDataRace2() {
        FrameBuilder<IAccess, Integer> task1 = new FrameBuilder<>(100);
        IView event = new IView(100);
        AsyncLocationTracker<IAccess, Integer> loc = new AsyncLocationTracker<>();
        task1.add(loc, new AccessEntry<>(IAccess.WRITE, event), AsyncLocationTracker.TICKET_ZERO);
        Frame<IAccess, Integer> frame1 = task1.build();

        // This access happened after and does not observe first write
        FrameBuilder<IAccess, Integer> task2 = new FrameBuilder<>(100);
        task2.add(loc, new AccessEntry<>(IAccess.WRITE, new IView(40)), AsyncLocationTracker.TICKET_ZERO + 1);
        Frame<IAccess, Integer> frame2 = task2.build();

        // Interleave 1
        ShadowMemory<IAccess, Integer> mem = new ShadowMemory<>();
        frame1.addTo(mem);
        try {
            frame2.addTo(mem);
            fail();
        } catch (IllegalStateException e) {
            // OK
        }
    }

    @Test
    public void addNoDataRace1() {
        FrameBuilder<IAccess, Integer> task1 = new FrameBuilder<>(100);
        AsyncLocationTracker<IAccess, Integer> loc = new AsyncLocationTracker<>();
        task1.add(loc, new AccessEntry<>(IAccess.WRITE, new IView(10)), 1);
        Frame<IAccess, Integer> frame1 = task1.build();

        // the event that happened on time-step 2 observed ( >= ) the first event
        FrameBuilder<IAccess, Integer> task2 = new FrameBuilder<>(100);
        task2.add(loc, new  AccessEntry<>(IAccess.WRITE, new IView(40)), 2);
        Frame<IAccess, Integer> frame2 = task2.build();

        {
            // Interleave 1:
            ShadowMemory<IAccess, Integer> mem = new ShadowMemory<>();
            frame1.addTo(mem);
            frame2.addTo(mem);
        }

        {
            // Interleave 2:
            ShadowMemory<IAccess, Integer> mem = new ShadowMemory<>();
            frame2.addTo(mem);
            frame1.addTo(mem);
        }
    }
}