package balok.causality.async;

import balok.causality.Causality;
import balok.causality.Event;

import java.util.Optional;

final class IView implements Event<Integer> {
    public IView(Integer value) {
        this.value = value;
    }

    private Integer value;

    @Override
    public Integer project() {
        return value;
    }

    @Override
    public Optional<Causality> observe(Integer o) {
        int result = value.compareTo(o);
        if (result == 0) return Optional.of(Causality.EQ);
        if (result > 0) return Optional.of(Causality.HAPPENED_AFTER);
        return Optional.empty();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IView iView = (IView) o;

        return value != null ? value.equals(iView.value) : iView.value == null;
    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
