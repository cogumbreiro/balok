package balok.causality.async;

import com.google.common.collect.Collections2;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

import static org.junit.Assert.*;

public class TestIntervalTree {
    private static class I implements Consumer<I> {
        public I(int value) {
            this.value = value;
        }

        int value;
        @Override
        public void accept(I other) {
            this.value = Math.max(this.value, other.value);
        }

        @Override
        public String toString() {
            return Integer.toString(value);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            I i = (I) o;
            return value == i.value;
        }

        @Override
        public int hashCode() {

            return Objects.hash(value);
        }
    }

    @Test
    public void constructor() {
        I i10 = new I(10);
        IntervalTree<I> rng1 = new IntervalTree<>(i10, new Interval(1));
        assertSame(i10, rng1.getValue());
        assertSame(1, rng1.getInterval().getLowEndpoint());
        assertSame(2, rng1.getInterval().getHighEndpoint());
        assertNull(rng1.getBiggerRange());
        assertNull(rng1.getSmallerRange());
    }

    @Test
    public void happyPath() {
        IntervalTree<I> rng1 = new IntervalTree<>(new I(10), new Interval(1));
        IntervalTree<I> rng2 = new IntervalTree<>(new I(20), new Interval(2));
        rng1.add(rng2);
        assertEquals(20, rng1.getValue().value);
        assertEquals(new Interval(1, 3), rng1.getInterval());
        assertNull(rng1.getBiggerRange());
        assertNull(rng1.getSmallerRange());
    }

    private static void runPerms(int count, Consumer<IntervalTree[]> cb) {
        ArrayList<Integer> elems = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            elems.add(i);
        }
        // Try all the permutations of adding the tree events
        for (List<Integer> perm : Collections2.orderedPermutations(elems)) {
            IntervalTree[] ranges = new IntervalTree[count];
            for (Integer i : perm) {
                ranges[i] = new IntervalTree<>(new I((i + 1)*10), new Interval(i + 1));
            }
            cb.accept(ranges);
        }
    }

    @Test
    public void add4() {
        runPerms(4, rng -> {
            // Two groups of intervals connecting with each other:
            rng[0].add(rng[1]);
            rng[2].add(rng[3]);
            rng[0].add(rng[2]);
            assertEquals(rng[0].toString(), new IntervalTree(new I(40), new Interval(1, 5)), rng[0]);
            assertNull(rng[0].getSmallerRange());
            assertNull(rng[0].getBiggerRange());
        });
    }
    @Test
    public void addN() {
        // merge all elements into one, check all possible permutations
        for (int i = 1; i < 7; i++) {
            final int count = i;
            runPerms(count, rng -> {
                IntervalTree r = rng[0];
                for (int j = 1; j < count; j++) {
                    r.add(rng[j]);
                }
                assertEquals(rng[0].toString(), new IntervalTree(new I(count * 10), new Interval(1, count + 1)), rng[0]);
                assertNull(rng[0].getSmallerRange());
                assertNull(rng[0].getBiggerRange());
            });
        }
    }
}
