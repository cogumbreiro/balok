package balok.causality;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Collection;

@RunWith(Parameterized.class)
public class TestTaskTracker {
    TaskTracker task1;
    TaskTracker task2;
    PtpCausalityFactory factory;

    @Parameterized.Parameters(name = "{0}")
    public static Collection<Object[]> data() {
        Collection<Object[]> result = new ArrayList<>();
        for (PtpCausalityFactory factory : PtpCausalityFactory.values()) {
            result.add(new Object[]{factory});
        }
        return result;
    }

    public TestTaskTracker(PtpCausalityFactory factory) {
        this.factory = factory;
    }

    private TaskTracker createTaskTracker() {
        return new TaskTracker(factory.createController());
    }

    @Test
    public void example1() {
        task1 = createTaskTracker();
        task1.produceEvent(); // init
        task2 = task1.createChild();
        // spawn happens before the child
        Util.assertHB(task1.createTimestamp(), task2.createTimestamp());
        task1.afterSpawn();
        // after the spawn both tasks are concurrent
        Util.assertPAR(task1.createTimestamp(), task2.createTimestamp());
    }

    @Test
    public void produceEvent() {
        task1 = createTaskTracker();
        task1.produceEvent(); // init
        TaskView s1 = task1.createTimestamp();
        task1.produceEvent();
        TaskView s2 = task1.createTimestamp();
        Util.assertHB("s1 < s2", s1, s2);
    }

    @Test
    public void barrier1() {
        task1 = createTaskTracker();
        task1.produceEvent(); // init
        AllToAllTracker barrier = task1.createBarrier();
        TaskView s1 = task1.createTimestamp();
        barrier.signalAwait();
        Util.assertHB(s1, task1.createTimestamp());
    }

    @Test
    public void phaser1() {
        task1 = createTaskTracker();
        task1.produceEvent(); // init
        TaskView s1 = task1.createTimestamp();
        PhaserTracker barrier = task1.createPhaser();
        Util.assertHB(s1, task1.createTimestamp());
    }

    @Test
    public void barrier2() {
        task1 = createTaskTracker();
        task1.produceEvent(); // init
        TaskView s1 = task1.createTimestamp();
        PhaserTracker barrier = task1.createPhaser();
        barrier.signal();
        TaskView s2 = task1.createTimestamp();
        Util.assertHB(s1, s2);
    }

    @Test
    public void futures2Example() {
        TaskTracker root = createTaskTracker();
        root.produceEvent();
        TaskView s0 = root.createTimestamp();
        // s0 -F-> sx
        // s0 -C-> s1
        TaskTracker fx = root.createChild();
        TaskView sx = fx.createTimestamp();
        root.afterSpawn();
        TaskView s1 = root.createTimestamp();
        Util.assertHB("s0 < sx", s0, sx);
        Util.assertHB("s0 < s1", s0, s1);
        // s1 -F-> sy
        // s1 -C-> s2
        TaskTracker fy = root.createChild();
        TaskView sy = fy.createTimestamp();
        root.afterSpawn();
        TaskView s2 = root.createTimestamp();
        Util.assertHB("s1 < sy", s1, sy);
        Util.assertHB("s1 < s2", s1, s2);
        // sx -J-> s3
        // s2 -C-> s3
        root.join(fx.createTimestamp());
        TaskView s3 = root.createTimestamp();
        Util.assertHB("sx < s3", sx, s3);
        Util.assertHB("s2 < s3", s2, s3);
        // sy -J-> s4
        // s3 -C-> s4
        root.join(fy.createTimestamp());
        TaskView s4 = root.createTimestamp();
        Util.assertHB("sy < s4", sy, s4);
        Util.assertHB("s3 < s4", s3, s4);
        // Others
        Util.assertPAR("s1 || sx", s1, sx);
        Util.assertPAR("sx || sy", sx, sy);
        Util.assertPAR("s2 || sy", s2, sy);
    }

}
